import java.io.*;
import java.util.*;
public class shiyan1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a;
        System.out.println("凯撒密码计算");
        do{
            System.out.println("1.加密  2.解密");//要执行的操作
            System.out.println("请输入要执行的操作");
            int n = in.nextInt();
            if(n == 1) {
                try {
                    EncryptAndDecrypt person = new EncryptAndDecrypt();
                    System.out.println("明文：");
                    Scanner scan = new Scanner(System.in);
                    String sourceString = scan.next();
                    System.out.println("密钥:");
                    Scanner scanner = new Scanner(System.in);
                    int password = scanner.nextInt();
                    String secret = person.encrypt(sourceString, password);
                    System.out.println("密文:" + secret);
                }
                catch(EncryptAndDecryptException e) {
                    System.out.println("Warning:");
                    System.out.println(e.warnMess());
                }
            }
            else if(n == 2) {
                try {
                    EncryptAndDecrypt person = new EncryptAndDecrypt();
                    System.out.println("密文:");
                    Scanner scan = new Scanner(System.in);
                    String sourceString = scan.next();
                    System.out.println("密钥：");
                    Scanner scanner = new Scanner(System.in);
                    int password = scanner.nextInt();
                    String source = person.decrypt(sourceString,password);
                    System.out.println("明文:"+source);
                }
                catch(EncryptAndDecryptException e) {
                    System.out.println("Warning:");
                    System.out.println(e.warnMess());
                }
            }
            System.out.println("输入1继续，输入0结束：");
            a = in.nextInt();
        }while(a==1);
    }
}

class EncryptAndDecrypt {
    String encrypt(String sourceString,int password) throws EncryptAndDecryptException { //加密算法
        char [] c = sourceString.toCharArray();
        int m = c.length;
        for(int k=0;k<m;k++){
            if((c[k]>=65&&c[k]<=90)||(c[k]>=97&&c[k]<=122)){
                int mima=(c[k] -'a' + password) % 26 + 'a';       //加密,同时转码
                c[k]=(char)mima;
            }
            else {
                throw new EncryptAndDecryptException(sourceString);
            }
        }
        return new String(c);    //返回密文
    }
    String decrypt(String sourceString,int password) throws EncryptAndDecryptException { //解密算法
        char [] c = sourceString.toCharArray();
        int m = c.length;
        for(int k=0;k<m;k++){
            if((c[k]>=65&&c[k]<=90)||(c[k]>=97&&c[k]<=122)){
                int mima=((c[k] + 26) -'a' - password) % 26 + 'a';       //解密,同时转码
                c[k]=(char)mima;
            }
            else {
                throw new EncryptAndDecryptException(sourceString);
            }
        }
        return new String(c);    //返回明文
    }
}

class EncryptAndDecryptException extends Exception {
    String message;
    public EncryptAndDecryptException(String sourceString) {
        message="输入错误！";
    }
    public String warnMess() {
        return message;
    }
}