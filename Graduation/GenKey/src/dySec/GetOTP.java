package dySec;
public class GetOTP {
    public String getOTP(long T, int C, String Q, String key) {
        SM4 sm4 = new SM4();
        String S = sm4.getS(T,C,Q,key);  //获取一次性中间值
        Password pwd = new Password();
        String P = pwd.getP(S);   //获取动态口令
        System.out.println("P = " + P);
        return P;
    }
}