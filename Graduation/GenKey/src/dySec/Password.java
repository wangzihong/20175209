package dySec;
public class Password {
    public String getP(String S) {
        long S1 = HexToO(S.substring(0,8));
        long S2 = HexToO(S.substring(8,16));
        long S3 = HexToO(S.substring(16,24));
        long S4 = HexToO(S.substring(24,32));       //获取分组S1—S4
        long OD = (S1 + S2 + S3 +S4) % Math.round(Math.pow(2,32));
        String P = String.format("%6s",String.valueOf(OD % Math.round(Math.pow(10,6)))).replace(' ','0');
        return P;
    }

    public static long HexToO(String num) {
        long result=0;
        int base;
        int j=num.length()-1;
        for(int i=0 ; i<num.length() ; i++ ) {
            String temp = String.valueOf(num.charAt(j--));
            switch (temp) {
                case "a":
                    base = 10;
                    break;
                case "b":
                    base = 11;
                    break;
                case "c":
                    base = 12;
                    break;
                case "d":
                    base = 13;
                    break;
                case "e":
                    base = 14;
                    break;
                case "f":
                    base = 15;
                    break;
                    default:
                        base = Integer.parseInt(temp);

            }
            double a = Math.pow(16,i);
            result += a*base;
        }
        return result;
    }
}

