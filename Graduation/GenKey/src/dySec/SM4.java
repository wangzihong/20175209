package dySec;

public class SM4 {
    public String getS(long T, int C, String Q, String key) {
        String HexT = String.format("%16s", Long.toHexString(T)).replace(' ', '0');
        String HexC = String.format("%8s", Integer.toHexString(C)).replace(' ', '0');
        String HexQ = new String();
        String ID = null;
        for(int i = 0; i < Q.length(); i++) {
            int ch = Q.charAt(i);
            HexQ += Integer.toHexString(ch);
        }
        if (C == 0){    //   T||Q||0
            ID = HexT + HexQ + "00000000";
        }
        else if (T == 0) {    //  C||Q||0
            ID = HexC + HexQ + "0000000000000000";
        }
        else {      //   T||C||Q
            ID = HexT + HexC + HexQ;
        }
        System.out.println("十六进制组装序列为：" + ID);
        GainS input = new GainS();
        String output = input.getS(ID,key);    //计算一次性中间值S
        return output;
    }
}
