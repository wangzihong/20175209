package dySec;
import java.util.Scanner;

public class TestMain {
    public static void main(String[] args) {
        /*  T,C,Q 是生成动态口令的三个因子，其中T是时间因子，C是事件因子，Q是挑战因子；
        T是登录的本地时间，您写令牌时可以直接取“1340783053”这个数；
        C就是一个内置的计数器，初始值为1，用户每生成完一次口令，C+1；
        Q就是需要输入的四位随机数；
        key是种子密钥（这里是未被加密过，即未被保护的种子密钥，是32位的16进制数）*/

        //int T = 1340783053;
        int T;  //时间因子
        int C;   //事件因子
        String Q;  //挑战因子
        String key;  //种子密钥
        Scanner Int = new Scanner(System.in);
        Scanner string = new Scanner(System.in);
        System.out.print("请输入时间因子T：");
        T = Int.nextInt();
        System.out.print("请输入事件因子C：");
        C = Int.nextInt();
        System.out.print("请输入挑战因子Q：");
        Q = string.next();
        System.out.print("请输入加密密钥key：");
        key = string.next();
        GetOTP OTP = new GetOTP();
        String P = OTP.getOTP(T,C,Q,key);     //生成的动态口令
    }
}
