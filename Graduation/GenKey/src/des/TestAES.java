package des;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class TestAES {
    public static void main(String[] args) throws NoSuchPaddingException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        AES aes = new AES();
        String UserKey = "1234567890abcdef1234567890abcdef";
        String message = "e10adc3949ba59abbe56e057f20f883e";
        String EncryptMess = aes.aesEncry(message, UserKey);
        System.out.println("EncryptMess = " + EncryptMess);
        String DecryptMess = aes.aesDecry(EncryptMess, UserKey);
        System.out.println("EncryptMess = " + DecryptMess);
    }
}
