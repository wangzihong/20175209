package des;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import java.util.Properties;

/**
 * 修改面界面
 *
 */
public class UpdatePasswordFrame extends Application {

    private Properties prop;

    private PasswordField passwordInput; //密码输入框
    private PasswordField reInput;   //确认密码输入框
    private Stage stage;

    //信息提示文本框
    public Label info;

    public UpdatePasswordFrame(Properties properties) {
        this.prop=properties;
    }

    /**
     * 启动程序
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * 启动界面
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("设置密码");
        this.stage=primaryStage;
            //界面布局
            GridPane grid = new GridPane();
            grid.setAlignment(Pos.CENTER);  //设置对其属性
            grid.setHgap(10);    //设置水平间距
            grid.setVgap(10);    //设置垂直间距
            grid.setPadding(new Insets(25,25,25,25));  //设置内边距属性用来管理节点元素和GridPane边缘之间的距离

            Label initPrompt = new Label("首次登录请修改密码：");
            grid.add(initPrompt,0,0,2,1);

            Label initNumLabel = new Label("密码");
            grid.add(initNumLabel,0,1);

            passwordInput = new PasswordField();
            grid.add(passwordInput,1,1);
            //隔断一行
            grid.add(new Label("确认密码"),0,3);

            reInput = new PasswordField();
            grid.add(reInput,1,3);

            Button startBtn = new Button("保存");
            startBtn.setOnAction(actionEvent -> savePassword());

            HBox hbBtn = new HBox(10);
            hbBtn.setAlignment(Pos.BOTTOM_LEFT);
            hbBtn.getChildren().add(startBtn);
            grid.add(hbBtn,1,4);

            //显示提示的文本框
            info = new Label();
            grid.add(info,1,5);
            //显示界面
            Scene scene = new Scene(grid,350,200);
            primaryStage.setScene(scene);
            primaryStage.show();
    }

    /**
     * 保存密码
     * @return
     */
    private void savePassword() {
        String InPwd=passwordInput.getText();
        if (InPwd==null || "".equals(InPwd)) {
            info.setText("请输入密码！");
            return;
        }
        String rePassword=reInput.getText();
        if (rePassword==null || "".equals(rePassword)) {
            info.setText("请输入确认密码！");
            return;
        }
        if (!InPwd.equals(rePassword)) {
            info.setText("两次密码不一致！");
            return;
        }

        prop.setProperty("initPassword","true");
        System.out.println(prop.getProperty("c"));
        if (prop.getProperty("c").equals("")) {
            prop.setProperty("c","0");
        }
        String PA = new MD5().getMD5String(InPwd);
        PropUtils.storeConfig(prop,PA);
        PropUtils.saveConfig(0);
        //打开登录界面
        LoginFrame open = new LoginFrame(prop);
        try {
            open.start(new Stage());
            stage.hide();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
