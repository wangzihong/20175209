package des;

import javafx.application.Application;
import javafx.stage.Stage;

import java.util.Properties;

public class LoginMain extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Properties prop=PropUtils.loadConfig();
        String initPassword=prop.getProperty("initPassword");
        Application open = new LoginFrame(prop);
        MD5 md5 = new MD5();
        if (!"true".equals(initPassword)) {
            //跳转到设置密码页面
            String pa0="000000";
            String password = md5.getMD5String(pa0);
            PropUtils.storeConfig(prop,password);
            open = new LoginFrame(prop);
        } else {
            open = new LoginFrame(prop);
        }
        try {
            open.start(new Stage());
            primaryStage.hide();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
