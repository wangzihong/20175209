package des;

import dySec.Time;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import sun.java2d.windows.GDIRenderer;

import java.util.Properties;


public class ChooseFrame extends Application {

    private Properties prop;
    private String key;
    private int c=0;
    private Stage stage;
    private long T=0;
    //测试结果文本框
    public Label info;
    Time time = new Time();

    public ChooseFrame(Properties properties, String genKey) {
        this.prop = properties;
        this.key = genKey;
    }
    /**
     * 启动程序
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.stage = primaryStage;
        primaryStage.setTitle("选择认证方式");  //设置窗口名称
            //界面布局
            GridPane grid = new GridPane();
            grid.setAlignment(Pos.CENTER);
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(25,25,25,25));

            Label initPrompt = new Label("请选择认证方式：");
            grid.add(initPrompt,0,0,2,1);

            Button startBtn = new Button("时间认证");
            startBtn.setMinSize(120,30);
            startBtn.setOnAction(actionEvent -> {
                try {
                    ChooseTime();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            HBox hbBtn = new HBox(20);
            hbBtn.setAlignment(Pos.CENTER);
            hbBtn.getChildren().add(startBtn);    //将按钮控件作为子节点
            grid.add(hbBtn,0,1);

            Button startBtn1 = new Button("事件认证");
            startBtn1.setMinSize(120,30);
            startBtn1.setOnAction(actionEvent -> {
                try {
                    ChooseCount();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            HBox hbBtn1 = new HBox(20);
            hbBtn1.setAlignment(Pos.CENTER);
            hbBtn1.getChildren().add(startBtn1);
            grid.add(hbBtn1,0,2);

            Button startBtn2 = new Button("时间+事件认证");
            startBtn2.setMinSize(120,30);
            startBtn2.setOnAction(actionEvent -> {
                try {
                    TimeAndCount();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            HBox hbBtn2 = new HBox(20);
            hbBtn2.setAlignment(Pos.CENTER);
            hbBtn2.getChildren().add(startBtn2);
            grid.add(hbBtn2,0,3);

            info = new Label();
            grid.add(info,0,4);
            Scene scene = new Scene(grid, 350,200);
            primaryStage.setScene(scene);
            primaryStage.show();
    }

    private void ChooseTime() {
        Application open = null;
        T = time.getUTCTimeStr();
        System.out.println("T = " + T);
        open = new MainFrame(T,c,key);
        try {
            info.setText("打开界面");
            open.start(new Stage());
            stage.hide();
        } catch (Exception e) {
            info.setText("异常：" + e.getMessage());
        }
    }

    private void ChooseCount() {
        Application open = null;
        String cstr=prop.getProperty("c");
        c = Integer.valueOf(cstr);
        info.setText("密码正确" + c);
        System.out.println("T = " + T);
        System.out.println("c = " + c);
        PropUtils.CountConfig(prop,String.valueOf(Integer.valueOf(c)));
        open = new MainFrame(T,c,key);
        try {
            info.setText("打开界面");
            open.start(new Stage());
            stage.hide();
        } catch (Exception e) {
            info.setText("异常：" + e.getMessage());
        }
    }

    private void TimeAndCount() {
        Application open = null;
        T = time.getUTCTimeStr();
        String cstr=prop.getProperty("c");
        c = Integer.valueOf(cstr);
        info.setText("密码正确" + c);
        System.out.println("T = " + T);
        System.out.println("c = " + c);
        PropUtils.CountConfig(prop,String.valueOf(Integer.valueOf(c)));
        open = new MainFrame(T,c,key);
        try {
            info.setText("打开界面");
            open.start(new Stage());
            stage.hide();
        } catch (Exception e) {
            info.setText("异常：" + e.getMessage());
        }
    }
}
