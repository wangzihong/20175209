package des;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

public class LoginFrame extends Application {

    private Properties prop;

    private TextField passwordInput;     //登录密码输入框
    private Stage stage;

    public Label info;

    public LoginFrame(Properties properties) {
        this.prop=properties;
    }

    /**
     * 启动程序
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * 启动界面
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.stage=primaryStage;
        primaryStage.setTitle("登录");   //设置窗口名称
            //界面布局
            GridPane grid = new GridPane();
            grid.setAlignment(Pos.CENTER);
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(25,25,25,25));

            Label initPrompt = new Label("请输入密码：");
            grid.add(initPrompt,0,0,2,1);

            Label initNumLabel = new Label("密码");
            grid.add(initNumLabel,0,1);

            passwordInput = new PasswordField();
            grid.add(passwordInput,1,1);

            Button startBtn = new Button("登录");
            startBtn.setOnAction((actionEvent -> {
                try {
                    startLogin();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }));

            HBox hbBtn = new HBox(10);
            hbBtn.setAlignment(Pos.BOTTOM_LEFT);
            hbBtn.getChildren().add(startBtn);
            grid.add(hbBtn,1,3);

            //显示请求进度的文本框
            info = new Label();
            grid.add(info,1,4);
            //显示查询界面
            Scene scene = new Scene(grid,350,200);
            primaryStage.setScene(scene);
            primaryStage.show();
    }

    /**
     * 登录验证
     * @return
     */
    private void startLogin() throws NoSuchPaddingException, UnsupportedEncodingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException {
        String In=passwordInput.getText();
        if (In==null || "".equals(In)) {
            info.setText("请输入密码！");
            return;
        }
        //验证密码是否正确
        String PB= new MD5().getMD5String(In);
        String PA=prop.getProperty("protectedPwd");
        if (!PA.equals(PB)) {
            info.setText("密码错误！");
            return;
        }
        //决定登录界面
        info.setText("开始登录！");
        String initPassword=prop.getProperty("initPassword");
        Application open = null;
        if (!"true".equals(initPassword)) {         //判断是否首次登录，首次登录需修改密码
            info.setText("修改密码！");
            open = new UpdatePasswordFrame(prop);
        } else {
            String EncryptKey=prop.getProperty("EncryptKey");
            String key = AES.aesDecry(EncryptKey,PB);
            open = new ChooseFrame(prop,key);
        }

        try {
            info.setText("打开界面");
            open.start(new Stage());
            stage.hide();
        } catch (Exception e) {
            info.setText("异常：" + e.getMessage());
        }
    }
}
