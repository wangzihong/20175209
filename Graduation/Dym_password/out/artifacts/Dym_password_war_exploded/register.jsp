<%@ page import="java.util.List" %>
<%@ page import="java.util.LinkedList" %><%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2021/4/1
  Time: 9:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>动态口令认证系统的设计与实现——注册</title>
    <link href="css/ufo.css" rel="stylesheet" type="text/css">
    <link href="css/lanrenzhijia.css" type="text/css" rel="stylesheet">
    <link href="css/login_p_1128.css" rel="stylesheet" type="text/css">
    <script src="js/hm_002.js"></script>
    <script src="js/hm.js"></script>
    <script src="js/jquery-1.js" type="text/javascript"></script>
    <script src="js/Login.js" type="text/javascript"></script>
    <script src="js/Common.js"></script>
    <script src="js/public.js"></script>
    <script src="js/encrypt.js" type="text/javascript"></script>
    <script src="js/wait.js" type="text/javascript"></script>
    <style type="text/css">
        body {
            width: 100%;
            margin: 0;
            font-size: 12px;
            font-family: "微软雅黑", "宋体";
            color: #1f1e23;
            text-align: center;
        }
        #footer {
            width: 980px;
            margin: 0 auto;
            text-align: center;
        }
        #footer p {
            font-size: 12px;
            color: #666;
            font-family: Arial, "微软雅黑";
            display: block;
            margin-top: 5px;
        }
        .footer_title {
            font-weight: bold;
            color: #666;
            font-size: 14px;
            line-height: 2em;
        }
    </style>
    <script>
        var _hmt = _hmt || [];
        (function () {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?a4f7c7f81f672b97209121582bca25c7";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    <script>
        var _hmt = _hmt || [];
        (function () {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?2e4ea7a3aed8b97b0a4e900eb4eaa7e6";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
</head>
<body>
<script>
    var label = '${result}';
    if (label == "isExist") {
        alert("用户名已被使用！")
    }
</script>
<% List<String> IDList = (LinkedList<String>)request.getSession().getAttribute("KeyID");%>
<form id="form1" method="post" action="Registex">
    <input name="__RequestVerificationToken" type="hidden" value="0amhNH4v6Ks6djmiZoRVf3EcZoUQ7BGoayoGpEEr5gu1E9YKr_-SG8dCG4ad9SaUixbqYTjM8YFH1bm5sb5qNPHD5OylMeFSqmCHtsZsDoU1">
    <div style="width:100%;background:url(http://resource.neea.edu.cn/project/Passport/Images/top_logo.jpg) repeat-x; ">
        <div id="top">
            <div id="logo">
                <div class="">
                    <ul id="logo_nav">
                        <li><a href="#">客服电话：</a></li>
                    </ul>
                </div>
                <div style="clear:both"></div>
                <div class="huangs02">010-62987880</div>
            </div>
            <div id="nav">
                <div style="width:980px; text-align:center;">
                </div>
            </div>
        </div>
    </div>
    <div class="login_box" style="margin-top:25px;">
        <div class="login_form">
            <img src="image/login_title.jpg" width="500" height="67">
            <div class="login_form_box" style="margin-left:0;margin-top:15px;">
                <div class="loginForm_res_01">
                    <label class="#" id="pwdInput">用户名：</label>
                    <input class="input_box" type="text" required="required" name="username" title="请输入用户名" placeholder="输入用户名" style="color: gray;background-color: rgb(255,255,255)">
                </div>
                <div class="loginForm_res_02">
                    <label class="#" for="pwdInput">密码：</label>
                    <input class="input_box" type="password" required="required" name="password" title="请输入密码" placeholder="首次输入密码" style="color: gray;background-color: rgb(255,255,255)">
                </div>
                <div class="loginForm_res_03">
                    <label class="#" for="pwdInput">确认密码：</label>
                    <input class="input_box" type="password" required="required" name="password" title="请确认密码" placeholder="再次确认密码" style="color: gray;background-color: rgb(255,255,255)">
                </div>
                <div class="loginForm_res_04">
                    <label>请选择令牌：</label>
                    <select class="select_box" name="KeyID" id = "Key">
                        <% for(int i = 0; i<IDList.size(); i++) { %>
                        <option><%out.println(IDList.get(i));%></option>
                        <% } %>
                    </select>
                </div>
                <div id="button" style="margin-top:5px;">
                    <button class="button_onmouseout" name="LoginButton" actioncontrol="1" onmousemove="this.className='button_onmousemove'"
                            onmouseout="this.className='button_onmouseout'" type="submit">
                        注册
                    </button>
                    &nbsp;
                    <button class="button_onmouseout" name="LoginButton" actioncontrol="1" onmousemove="this.className='button_onmousemove'"
                            onmouseout="this.className='button_onmouseout'" type="reset">
                        重置
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div id="footer">
        <div id="">
            <p>
                <span class="footer_title">COPYRIGHT 中华人民共和国教育部考试中心 All RIGHTS RESERVED</span><br>
                京ICP备05031027号<br>
            </p>
        </div>
    </div>
    <input id="hdnForce" name="hdnForce" value="0" type="hidden">
    <input id="hdnLoginMode" name="hdnLoginMode" type="hidden">
    <input id="hdnReturnUrl" name="hdnReturnUrl" value="http://cet-bm.neea.edu.cn/Home/VerifyPassport/?LoginType=0" type="hidden">
    <input id="hdnRedirectUrl" name="hdnRedirectUrl" type="hidden">
    <input id="HiddenAccessToken" name="HiddenAccessToken" type="hidden">
    <input id="HiddenPublicKeyExponent" value="VNIkNZNytAH77HAt" name="HiddenPublicKeyExponent" type="hidden">
    <input id="HiddenPublicKeyModulus" value="NFsMFRlob0jrrj0b" name="HiddenPublicKeyModulus" type="hidden">
    <input id="HiddenSafe" name="HiddenSafe" value="0" type="hidden">
</form>
<script>
    function setForceCheckValue() {
        if ($("input[name='chkForce']:checked")) {
            $("#HiddenSafe").val(1);

        } else {
            $("#HiddenSafe").val(0);
        }
    }
</script>


</body><div id="edge-translate-notifier-container" class="edge-translate-notifier-center"></div>
</html>