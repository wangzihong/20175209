<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2021/3/30
  Time: 14:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>动态口令认证系统的设计与实现——登录</title>
    <link href="css/ufo.css" rel="stylesheet" type="text/css">
    <link href="css/lanrenzhijia.css" type="text/css" rel="stylesheet">
    <link href="css/login_p_1128.css" rel="stylesheet" type="text/css">
    <script src="js/hm_002.js"></script>
    <script src="js/hm.js"></script>
    <script src="js/jquery-1.js" type="text/javascript"></script>
    <script src="js/Login.js" type="text/javascript"></script>
    <script src="js/Common.js"></script>
    <script src="js/public.js"></script>
    <script src="js/encrypt.js" type="text/javascript"></script>
    <script src="js/wait.js" type="text/javascript"></script>
    <style type="text/css">
        body {
            width: 100%;
            margin: 0;
            font-size: 12px;
            font-family: "微软雅黑", "宋体";
            color: #1f1e23;
            text-align: center;
        }
        #footer {
            width: 980px;
            margin: 0 auto;
            text-align: center;
        }
        #footer p {
            font-size: 12px;
            color: #666;
            font-family: Arial, "微软雅黑";
            display: block;
            margin-top: 5px;
        }
        .footer_title {
            font-weight: bold;
            color: #666;
            font-size: 14px;
            line-height: 2em;
        }
    </style>
    <script>
        var _hmt = _hmt || [];
        (function () {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?a4f7c7f81f672b97209121582bca25c7";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    <script>
        var _hmt = _hmt || [];
        (function () {
            var hm = document.createElement("script");
            hm.src = "https://hm.baidu.com/hm.js?2e4ea7a3aed8b97b0a4e900eb4eaa7e6";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
</head>
<body>
<script>
    var label = '${message}';
    if (label == "success") {
        alert("恭喜您注册成功\n您的初始登录密码为000000");
    }
    if (label == "用户不存在！请您先注册") {
        alert("您尚未注册，请注册后登录！");
    }
    if (label == "密码错误") {
        alert("密码错误！");
    }
    if (label == "登录超时") {
        alert("登录已超时，请重新登录！");
    }
</script>
<form id="form1" method="post" action="">
    <input name="__RequestVerificationToken" type="hidden" value="0amhNH4v6Ks6djmiZoRVf3EcZoUQ7BGoayoGpEEr5gu1E9YKr_-SG8dCG4ad9SaUixbqYTjM8YFH1bm5sb5qNPHD5OylMeFSqmCHtsZsDoU1">
    <div style="width:100%;background:url(http://resource.neea.edu.cn/project/Passport/Images/top_logo.jpg) repeat-x; ">
        <div id="top">
            <div id="logo">
                <div class="">
                    <ul id="logo_nav">
                        <li><a href="#">客服电话：</a></li>
                    </ul>
                </div>
                <div style="clear:both"></div>
                <div class="huangs02">010-62987880</div>
            </div>
            <div id="nav">
                <div style="width:980px; text-align:center;">
                </div>
            </div>
        </div>
    </div>
    <div class="login_box" style="margin-top:25px;">
        <div class="login_form">
            <img src="image/login_title.jpg" width="470" height="63">
            <div class="login_form_box" style="margin-left:0;margin-top:15px;">
                <div class="loginForm_01">
                    <label class="#" id="pwdInput">用户名：</label>
                    <input class="input_box" type="text" name="username" title="请输入用户名" placeholder="用户名" style="color: gray;background-color: rgb(255,255,255)">
                </div>
                <div class="loginForm_02">
                    <label class="#" for="pwdInput">密码：</label>
                    <input class="input_box" type="password" name="password" title="请输入密码" placeholder="密码" style="color: gray;background-color: rgb(255,255,255)">
                </div>
                <div class="loginForm_03">
                    <label class="#" for="pwdInput">登录方式：</label>
                    <input class="input_box_02" type="radio" name="LoginMethod" value="TimeChallenge" title="时间+验证码"><label class="#">时间</label>&nbsp;&nbsp;
                    <input class="input_box_02" type="radio" name="LoginMethod" value="EventChallenge" title="事件+验证码"><label class="#">事件</label>&nbsp;&nbsp;
                    <input class="input_box_02" type="radio" name="LoginMethod" value="TimeAndEventChallenge" title="时间+事件+验证码"><label class="#">时间+事件</label>
                </div>
                <div class="loginForm_01">
                    <label class="#" for="pwdInput">验证码：</label>
                    <input class="input_box_01" type="text" name="random" title="请输入验证码" placeholder="验证码" style="color: gray;background-color: rgb(255,255,255)">
                    <canvas id="canvas" width="100" height="40" title="点击可更换验证码" style="vertical-align: bottom; width: 80px; height: 22px; background-color: rgb(255, 255, 255); color: rgb(0, 0, 0);margin: 0 auto;"></canvas>
                </div>
                <script type="text/javascript">
                    var myCanvas = document.querySelector("#canvas");
                    var blur = myCanvas.getContext("2d");
                    // 当点击画布时创建一个新的路径
                    // 验证码封装
                    myCanvas.onclick = function () {
                        // 实现点击画布创建一个新的验证码
                        blur.clearRect(0, 0, 100, 40);
                        verify();
                    }
                    verify();

                    function verify() {
                        // 绘制矩形框
                        // blur.strokeRect(0,0,100,40);
                        // 随机验证码
                        var arr = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
                        // 显示四位数的验证码
                        for (var i = 0; i < 4; i++) {
                            var x = 20 + 20 * i;
                            var y = 20 + 10 * Math.random();
                            // console.log(x,y);
                            // 在数组中获取到随机的索引整数
                            var index = Math.floor(Math.random() * arr.length);
                            // 通过随机的索引获取到随机的元素
                            var texts = arr[index];
                            // 设置验证码的相关样式
                            blur.font = "bold 20px 微软雅黑";
                            blur.fillStyle = textColor();
                            // 画布旋转显示
                            // translate() 方法重新映射画布上的位置
                            blur.translate(x, y);
                            // 将画布旋转，旋转角度，以弧度旋转(弧度=degrees*Math.PI/180 )
                            var deg = 90 * Math.random() * Math.PI / 180;
                            blur.rotate(deg); //
                            blur.fillText(texts, 0, 0);
                            // 将画布映射返回原来的位置
                            blur.rotate(-deg);
                            blur.translate(-x, -y);
                        }
                        // 制作验证码的干扰线制作
                        for (var i = 0; i < 6; i++) {
                            blur.beginPath();
                            blur.moveTo(Math.random() * 100, Math.random() * 40);
                            blur.lineTo(Math.random() * 100, Math.random() * 40);
                            // 设置干扰线的颜色
                            blur.strokeStyle = textColor();
                            blur.stroke();
                        }
                        // 制作验证码的干扰圆点
                        for (var i = 0; i < 20; i++) {
                            blur.beginPath();
                            var x = Math.random() * 100;
                            var y = Math.random() * 100;
                            blur.moveTo(x, y);
                            blur.lineTo(x + 1, y + 1);
                            // 设置干扰线的颜色
                            blur.strokeStyle = textColor();
                            blur.stroke();
                        }
                    }
                    // 获取随机颜色封装
                    function textColor() {
                        var red = Math.floor(Math.random() * 256);
                        var green = Math.floor(Math.random() * 256);
                        var blue = Math.floor(Math.random() * 256);
                        return "rgb(" + red + "," + green + "," + blue + ")";
                    }
                </script>
                <div class="loginForm_03">
                    <label>登录口令：</label>
                    <input class="input_box" type="text" name="Dym_pwd" title="请输入口令" placeholder="本次登录口令" style="color: gray;background-color: rgb(255,255,255)">
                </div>
                <div id="button" style="margin-top:5px;">
                    <button class="button_onmouseout" name="LoginButton" actioncontrol="1" onmousemove="this.className='button_onmousemove'"
                            onmouseout="this.className='button_onmouseout'" type="submit" onclick="form1.action='Loginex';form1.submit();">
                        登　录
                    </button>
                    &nbsp;
                    <button class="button_onmouseout" name="LoginButton" actioncontrol="1" onmousemove="this.className='button_onmousemove'"
                            onmouseout="this.className='button_onmouseout'" type="submit" onclick="form1.action='KeyID';form1.submit();">
                        注册
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div id="footer">
        <div id="">
            <p>
                <span class="footer_title">COPYRIGHT 中华人民共和国教育部考试中心 All RIGHTS RESERVED</span><br>
                京ICP备05031027号<br>
            </p>
        </div>
    </div>
    <input id="hdnForce" name="hdnForce" value="0" type="hidden">
    <input id="hdnLoginMode" name="hdnLoginMode" type="hidden">
    <input id="hdnReturnUrl" name="hdnReturnUrl" value="http://cet-bm.neea.edu.cn/Home/VerifyPassport/?LoginType=0" type="hidden">
    <input id="hdnRedirectUrl" name="hdnRedirectUrl" type="hidden">
    <input id="HiddenAccessToken" name="HiddenAccessToken" type="hidden">
    <input id="HiddenPublicKeyExponent" value="VNIkNZNytAH77HAt" name="HiddenPublicKeyExponent" type="hidden">
    <input id="HiddenPublicKeyModulus" value="NFsMFRlob0jrrj0b" name="HiddenPublicKeyModulus" type="hidden">
    <input id="HiddenSafe" name="HiddenSafe" value="0" type="hidden">
</form>
<script>
    function setForceCheckValue() {
        if ($("input[name='chkForce']:checked")) {
            $("#HiddenSafe").val(1);

        } else {
            $("#HiddenSafe").val(0);
        }
    }
</script>


</body><div id="edge-translate-notifier-container" class="edge-translate-notifier-center"></div>
</html>