<%--
  Created by IntelliJ IDEA.
  User: lenovo
  Date: 2021/3/31
  Time: 14:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<canvas id="canvas" width="100" height="40" style="border: 1px solid red;display: block;margin: 0 auto;"></canvas>
</body>
<script type="text/javascript">
    var myCanvas = document.querySelector("#canvas");
    var blur = myCanvas.getContext("2d");
    // 当点击画布时创建一个新的路径
    // 验证码封装
    myCanvas.onclick = function() {
        // 实现点击画布创建一个新的验证码
        blur.clearRect(0,0,100,40);
        verify();
    }
    verify();
    function verify() {
        // 绘制矩形框
        // blur.strokeRect(0,0,100,40);
        // 随机验证码
        var arr = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        // 显示四位数的验证码
        for (var i = 0; i < 4; i++) {
            var x = 20 + 20 * i;
            var y = 20 + 10 * Math.random();
            // console.log(x,y);
            // 在数组中获取到随机的索引整数
            var index = Math.floor(Math.random() * arr.length);
            // 通过随机的索引获取到随机的元素
            var texts = arr[index];
            // 设置验证码的相关样式
            blur.font = "bold 20px 微软雅黑";
            blur.fillStyle = textColor();
            // 画布旋转显示
            // translate() 方法重新映射画布上的位置
            blur.translate(x, y);
            // 将画布旋转，旋转角度，以弧度旋转(弧度=degrees*Math.PI/180 )
            var deg = 90 * Math.random() * Math.PI / 180;
            blur.rotate(deg); //
            blur.fillText(texts, 0, 0);
            // 将画布映射返回原来的位置
            blur.rotate(-deg);
            blur.translate(-x, -y);
        }

        // 制作验证码的干扰线制作
        for(var i=0;i<6;i++){
            blur.beginPath();
            blur.moveTo(Math.random()*100,Math.random()*40);
            blur.lineTo(Math.random()*100,Math.random()*40);
            // 设置干扰线的颜色
            blur.strokeStyle=textColor();
            blur.stroke();
        }
        // 制作验证码的干扰圆点
        for(var i=0;i<20;i++){
            blur.beginPath();
            var x=Math.random()*100;
            var y=Math.random()*100;
            blur.moveTo(x,y);
            blur.lineTo(x+1,y+1);
            // 设置干扰线的颜色
            blur.strokeStyle=textColor();
            blur.stroke();
        }
    }
    // 获取随机颜色封装
    function textColor(){
        var red=Math.floor(Math.random()*256);
        var green=Math.floor(Math.random()*256);
        var blue=Math.floor(Math.random()*256);
        return "rgb("+red+","+green+","+blue+")";
    }
</script>
</html>
