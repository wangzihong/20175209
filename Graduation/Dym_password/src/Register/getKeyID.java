package Register;

import DBUtil.DBUtil;
import java.io.IOException;
import java.util.*;

public class getKeyID extends javax.servlet.http.HttpServlet{
    @Override
    protected void doPost(javax.servlet.http.HttpServletRequest request,javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        DBUtil dbUtil = new DBUtil();
        List<String> ID = new LinkedList<String>();
        try {
            ID = dbUtil.getKeyID();
        } catch (Exception e) {
            e.printStackTrace();
        }
        request.getSession().setAttribute("KeyID",ID);
        response.sendRedirect("register.jsp");
    }

}
