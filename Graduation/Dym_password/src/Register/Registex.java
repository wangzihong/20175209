package Register;

import DBUtil.DBUtil;
import UserKey.*;

public class Registex extends javax.servlet.http.HttpServlet{
    @Override
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) {
        DBUtil dbUtil = new DBUtil();
        Confusion confusion = new Confusion();
        String userName = request.getParameter("username");
        String passWord = request.getParameter("password");
        String KeyID = request.getParameter("KeyID");
        String ServerKey =  "edd03a512f05b4df67cbcd405a56d470";     //服务器密钥
        try {
            if (dbUtil.isExist(userName)) {
                request.getSession().setAttribute("result","isExist");
                response.sendRedirect("register.jsp");
            }
            else {
                boolean result = dbUtil.registerVerify(userName,passWord,KeyID);
                String UserKey = confusion.getConfusion(KeyID, ServerKey);
                System.out.println("服务器生成的用户密钥为：" + UserKey);
                //dbUtil.delete();
                if (result) {
                    dbUtil.UpdateKeyList(KeyID);
                    request.getSession().setAttribute("message","success");
                    response.sendRedirect("index.jsp");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
