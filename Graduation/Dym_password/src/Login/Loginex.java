package Login;

import DBUtil.DBUtil;
import UserKey.*;
import Verify.*;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class Loginex extends javax.servlet.http.HttpServlet{
    @Override
    protected void doPost(javax.servlet.http.HttpServletRequest request,javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        DBUtil dbUtil = new DBUtil();
        Time time = new Time();
        GetOTP OTP = new GetOTP();
        Confusion confusion = new Confusion();
        List<String> PList = new LinkedList<String>();
        int value = 0;
        int UserLoginCount = 0;
        long T = 0;
        String userName = request.getParameter("username");
        String passWord = request.getParameter("password");
        String LoginWay = request.getParameter("LoginMethod");
        String RandomCode = request.getParameter("random");       //传送随机因子 即 Q
        String DymPassword =request.getParameter("Dym_pwd");
        System.out.println(LoginWay);
        String ServerKey =  "edd03a512f05b4df67cbcd405a56d470";      //服务器密钥

        try {
            //验证用户存在性
            value = dbUtil.loginVerify(userName,passWord);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (value == -1) {
            request.getSession().setAttribute("message","用户不存在！请您先注册");;
            response.sendRedirect("index.jsp");
        }
        else if (value == 0) {
            request.getSession().setAttribute("message","密码错误");
            response.sendRedirect("index.jsp");
        }
        else if (value == 1) {
            try {
                boolean Conf = false;
                String ComputePwd = null;
                String KeyID = dbUtil.getUserKeyID(userName);
                String UserKey = confusion.getConfusion(KeyID, ServerKey);
                System.out.println("登录时计算的为：" + UserKey);
                // 时间认证方式，C为0；
                if (LoginWay.equals("TimeChallenge")) {
                    T = time.getUTCTimeStr();
                    System.out.println(T);
                    System.out.println(UserLoginCount);
                }
                //事件认证方式，T为0
                if (LoginWay.equals("EventChallenge")) {
                    T = 0;
                    UserLoginCount = dbUtil.GetLoginCount(userName);
                    System.out.println(T);
                    System.out.println(UserLoginCount);
                }
                if (LoginWay.equals("TimeAndEventChallenge")) {
                    T = time.getUTCTimeStr();
                    System.out.println(T);
                    UserLoginCount = dbUtil.GetLoginCount(userName);
                    System.out.println(UserLoginCount);
                }
                PList = OTP.getOTP(T,UserLoginCount,RandomCode,UserKey);
                if (LoginWay.equals("TimeChallenge")) {
                    for(int i = 0;i<5;i++) {
                        ComputePwd = PList.get(i);
                        System.out.println("登录时计算的为:" + ComputePwd);
                        if (ComputePwd.equals(DymPassword)) {
                            Conf = true;
                            break;
                        }
                    }
                }
                if (LoginWay.equals("EventChallenge")) {
                    ComputePwd = PList.get(0);
                    System.out.println("登录时计算的为:" + ComputePwd);
                    if (ComputePwd.equals(DymPassword)) {
                        dbUtil.UpdateLoginCount(userName,UserLoginCount+1);
                        System.out.println(UserLoginCount+1);
                        Conf = true;
                    }
                }
                if (LoginWay.equals("TimeAndEventChallenge")) {
                    for(int i = 0;i<5;i++) {
                        ComputePwd = PList.get(i);
                        System.out.println("登录时计算的为:" + ComputePwd);
                        if (ComputePwd.equals(DymPassword)) {
                            dbUtil.UpdateLoginCount(userName,UserLoginCount+1);
                            System.out.println(UserLoginCount+1);
                            Conf = true;
                            break;
                        }
                    }
                }
                if (Conf) {
                    request.getSession().setAttribute("userName",userName);
                    response.sendRedirect("success.jsp");
                }
                else {
                    request.getSession().setAttribute("message","登录超时");
                    response.sendRedirect("index.jsp");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


}
