package Verify;

public class testMain {
	public String getS(String input, String Key) {
		// TODO Auto-generated method stub
		// 此处给丁要进行加密的明文
		byte[] in = new byte[16];
		String output = new String();
		int j=0;
		for(int i=0;i<in.length;i++) {
			in[i] = hexToByte(input.substring(j,j+2));
			j=j+2;
		}
		// 此处给的加密密钥
		/*byte[] key = { 0x12, 0x34, 0x56, 0x78, (byte) 0x90, (byte) 0xab, (byte) 0xcd, (byte) 0xef, (byte) 0x12,
				(byte) 0x34, (byte) 0x56, (byte) 0x78, (byte)0x90, (byte)0xab, (byte)0xcd, (byte)0xef };*/
		byte[] key = new byte[16];
		j=0;
		//System.out.println(Key);
		for(int i=0;i<key.length;i++) {
			key[i] = hexToByte(Key.substring(j,j+2));
			j =j+2;
		}

		// 创建一个SMS4对象
		SMS4 sm4 = new SMS4();
		int inLen = 16, ENCRYPT = 1, DECRYPT = 0, inlen1 = 32;
		byte[] out = new byte[16];
		long starttime;
		// 加密 128bit
		starttime = System.nanoTime();
		sm4.sms4(in, inLen, key, out, ENCRYPT);
		//System.out.println("加密1个分组执行时间： " + (System.nanoTime() - starttime) + "ns");
		//System.out.println("明文：");
		/*for (int i = 0; i < 16; i++) {
			System.out.print(Integer.toHexString(in[i] & 0xff) + "\t");
		}
		System.out.println();*/

		/*System.out.println("加密密钥：");
		for (int i = 0; i < 16; i++) {
			System.out.print(Integer.toHexString(key[i] & 0xff) + "\t");
		}
		System.out.println();
		
		System.out.println("加密轮秘钥：");
		sm4.show_keyExtend(key,ENCRYPT );
		System.out.println();
		
		System.out.println("加密过程：");
		sm4.show(in , key,ENCRYPT);
		System.out.println();*/

		//System.out.println("加密结果：");
		for (int i = 0; i < 16; i++) {

			//System.out.print(Integer.toHexString(out[i] & 0xff) + "\t");
			output += byteToHex(out[i]);

		}
		/*System.out.println();
		
		System.out.println("解密轮秘钥：");
		sm4.show_keyExtend(key,DECRYPT );
		System.out.println();
	
		// 解密 128bit
		sm4.sms4(out, inLen, key, in, DECRYPT);

		System.out.println("解密结果：");

		for (int i = 0; i < 16; i++)
			System.out.print(Integer.toHexString(in[i] & 0xff) + "\t");*/

		// 加密多个分组

		/*System.out.println();

		sm4.sms4(in1, inlen1, key, out1, ENCRYPT);

		System.out.println("\r多分组加密结果：");

		for (int i = 0; i < 32; i++)

			System.out.print(Integer.toHexString(out1[i] & 0xff) + "\t");

		// 解密多个分组

		System.out.println();

		sm4.sms4(out1, inlen1, key, in1, DECRYPT);

		System.out.println("多分组解密结果：");

		for (int i = 0; i < 32; i++)

			System.out.print(Integer.toHexString(in1[i] & 0xff) + "\t");

		// 1,000,000次加密

		System.out.println();

		starttime = System.currentTimeMillis();

		for (int i = 1; i < 1000000; i++) {
			sm4.sms4(in, inLen, key, out, ENCRYPT);
			in = out;
		}
		sm4.sms4(in, inLen, key, out, ENCRYPT);

		System.out.println("\r1000000次加密执行时间： " + (System.currentTimeMillis() - starttime) + "ms");

		System.out.println("加密结果：");

		for (int i = 0; i < 16; i++)

			System.out.print(Integer.toHexString(out[i] & 0xff) + "\t");*/
		return output;
	}

	public static byte hexToByte(String inHex){
		return (byte)Integer.parseInt(inHex,16);
	}

	public static String byteToHex(byte b){
		String hex = Integer.toHexString(b & 0xFF);
		if(hex.length() < 2){
			hex = "0" + hex;
		}
		return hex;
	}
}
