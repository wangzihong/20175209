package Verify;

import java.util.Scanner;
import java.util.SplittableRandom;

public class SM4 {
    public String getS(Long T,int C, String Q, String key) {
//        int T = 1340783053;  //时间因子
//        int C;   //事件因子
//        String Q = "5678";  //挑战因子
//        String key;  //加密密钥
//        Scanner Int = new Scanner(System.in);
//        Scanner string = new Scanner(System.in);
//        System.out.print("请输入时间因子：");
//        T = Int.nextInt();
//        System.out.print("请输入事件因子：");
//        C = Int.nextInt();
//        System.out.print("请输入挑战因子：");
//        Q = string.next();
        String HexT = String.format("%16s", Long.toHexString(T)).replace(' ', '0');
        String HexC = String.format("%8s", Integer.toHexString(C)).replace(' ', '0');
        String HexQ = new String();
        String ID = null;
        for(int i = 0; i < Q.length(); i++) {
            int ch = Q.charAt(i);
            HexQ += Integer.toHexString(ch);
        }
        if (C == 0){    //   T||Q||0
            ID = HexT + HexQ + "00000000";
        }
        else if (T == 0) {    //  C||Q||0
            ID = HexC + HexQ + "0000000000000000";
        }
        else {      //   T||C||Q
            ID = HexT + HexC + HexQ;
        }
        /*String ID = HexT + HexC;
        for(int i = 0; i < Q.length(); i++) {
            int ch = Q.charAt(i);
            HexQ += Integer.toHexString(ch);
        }
        ID = ID + HexQ;          */      //十六进制表示组装序列ID
        System.out.println("十六进制组装序列ID表示为：" + ID);
//        System.out.print("请输入加密密钥：");
//        key = string.next();
//        key = "1234567890abcdef1234567890abcdef";
        testMain input = new testMain();
        String output = input.getS(ID,key);    //计算一次性中间值S
        //System.out.println(output);
        /*long time = System.currentTimeMillis();
        System.out.println(time);*/
        return output;
    }
}
