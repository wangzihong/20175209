package Verify;

import java.util.LinkedList;
import java.util.List;

public class GetOTP {
    public List<String> getOTP(long T, int C, String Q, String key) {
        SM4 sm4 = new SM4();
        Password pwd = new Password();
        String P = null;
        String S = null;
        List<String> PList = new LinkedList<String>();
        if (T!=0) {
            for(long i = 4;i>=0;i--) {
                S = sm4.getS(T-i,C,Q,key);  //获取一次性中间值
                P = pwd.getP(S);   //获取动态口令
                PList.add(P);
            }
        }
        else {
            S = sm4.getS(T,C,Q,key);
            P = pwd.getP(S);
            PList.add(P);
        }
        //System.out.println("本次的动态口令为：" + P);
        return PList;
    }
}
