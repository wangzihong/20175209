package UserKey;

public class Confusion {
    public String getConfusion(String KeyID, String ServerKey) {
        String confusion = new String();
        int i = 0;
        int length = KeyID.length() < ServerKey.length()? KeyID.length():ServerKey.length();
        while (length>=4) {
            confusion += KeyID.substring(i,i+4);
            confusion += ServerKey.substring(i,i+4);
            length -= 4;
            i += 4;
        }
        confusion += KeyID.substring(i);
        confusion += ServerKey.substring(i);
        MD5 md5 = new MD5();
        return md5.getMD5String(confusion);
    }
}
