package MySort;

import java.util.*;

public class MySort {
    public void mysort(String []toSort,int a) {
        System.out.println("Before sort:");
        for (String str: toSort) {
            System.out.println(str);
        }
        int length = toSort.length;
        if(a==0) {
            String [] num =new String[length];
            for(int i = 0;i<length;i++) {
                num[i] =toSort[i].split(":")[a];
            }
            Arrays.sort(num);
            System.out.println("After sort:");
            for(int i = 0;i<length;i++) {
                for(int j = 0;j<length;j++) {
                    if(num[i].equals(toSort[j].split(":")[a])) {
                        System.out.println(toSort[j]);
                    }
                }
            }
        }else {
            int[] num = new int[length];
            for (int i = 0; i < length; i++) {
                num[i] = Integer.parseInt(toSort[i].split(":")[a]);
            }
            Arrays.sort(num);
            System.out.println("After sort:");
            for (int i = 0; i < length; i++) {
                for (int j = 0; j < length; j++) {
                    if (num[i] == Integer.parseInt(toSort[j].split(":")[a])) {
                        System.out.println(toSort[j]);
                    }
                }
            }
        }
    }
}

