package MySQL;

import static java.lang.System.out;
import java.sql.*;

public class ConnectionDemo {
    public static void main(String[] args)
            throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        String jdbcUrl = "jdbc:mysql://localhost:3306/students?serverTimezone=GMT%2B8";
        String user = "root";
        String passwd = "wzh990213";
        try(Connection conn =
                    DriverManager.getConnection(jdbcUrl, user, passwd)) {
            out.printf("已%s数据库连接%n",
                    conn.isClosed() ? "关闭" : "打开");
        }
    }
}