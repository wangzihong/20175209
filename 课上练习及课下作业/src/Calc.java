import java.util.*;

public class Calc {
    public static void main(String[] args) {
        int result = 0;
        if(args.length != 3) {
            System.out.println("Usage: java Calc operato1 operand(+ - * / %) operator2");
        }
        result =Calculate.Calculate(Integer.parseInt(args[0]),args[1],Integer.parseInt(args[2]));
        System.out.println(args[0] + " " + args[1] + " " + args[2] + " = " + result);
    }
}

class Calculate {
    public static int Calculate(int num1,String op,int num2) {
        int result = 0;
        if(num2 == 0) {
            System.out.println("Wrong!The denominator can't be zero");
            System.exit(0);
        }
        switch(op) {
            case "+":
                result = num1 + num2;
                break;
            case "-":
                result = num1 - num2;
                break;
            case "x":
                result = num1 * num2;
                break;
            case "/":
                result = num1 / num2;
                break;
            case "%":
                result = num1 % num2;
                break;
            default:
                result = 0;
        }
        return result;
    }
}