package List;

import java.util.*;

public class MyList {
    public static void main(String [] args) {
        //选用合适的构造方法，用你学号前后各两名同学的学号创建四个结点
        Node<Integer> a = new Node<Integer>(20175207,null);
        Node<Integer> b = new Node<Integer>(20175208,null);
        Node<Integer> c = new Node<Integer>(20175210,null);
        Node<Integer> d = new Node<Integer>(20175211,null);
        //把上面四个节点连成一个没有头结点的单链表
        LinkedList<Node> mylist = new LinkedList<Node>();
        mylist.add(a);
        mylist.add(b);
        mylist.add(c);
        mylist.add(d);
        //遍历单链表，打印每个结点的
        Iterator<Node> point = mylist.iterator();
        System.out.println("初始链表为：");
        while(point.hasNext()) {
            String number = point.next().toString();
            System.out.print(number);
            if(point.hasNext()) {
                System.out.print("——>");
            }
        }
        System.out.println("\n");
        //把你自己插入到合适的位置（学号升序）
        Node<Integer> my = new Node<Integer>(20175209,null);
        mylist.add(2,my);
        //遍历单链表，打印每个结点的
        point= mylist.iterator();
        System.out.println("插入后的链表为：");
        while(point.hasNext()) {
            String number = point.next().toString();
            System.out.print(number);
            if(point.hasNext()) {
                System.out.print("——>");
            }
        }
        System.out.println("\n");
        //从链表中删除自己
        mylist.remove(2);
        //遍历单链表，打印每个结点的
        point= mylist.iterator();
        System.out.println("删除后的链表为：");
        while(point.hasNext()) {
            String number = point.next().toString();
            System.out.print(number);
            if(point.hasNext()) {
                System.out.print("——>");
            }
        }
    }
}

