import java.io.*;

public class MyCP {
    public static void main(String[] args) {
        String ch = args[0];
        String s1 = args[1];
        String s2 = args[2];
        File fromfile = new File(s1);
        File tofile = new File(s2);
        String result = "";
        try {
            FileReader in = new FileReader(fromfile);
            BufferedReader infile = new BufferedReader(in);
            FileWriter out = new FileWriter(tofile);
            BufferedWriter outfile = new BufferedWriter(out);
            String number = infile.readLine();
            if(ch.equals("-tx")) {
                int n;
                int number1 = Integer.parseInt(number);
                for(int i=number1;i>0;i=i/2) {
                    if(i%2==0)
                        n = 0;
                    else
                        n = 1;
                    result = n + result;
                }
            }
            else if(ch.equals("-xt")) {
                int number1 = Integer.parseInt(number,2);
                result = String.valueOf(number1);
            }
            outfile.write(result);
            outfile.flush();
            outfile.close();
            out.close();
        }
        catch(IOException e) {
            System.out.println("Error"+e);
        }
    }
}
