package ArrayInsertandDelete;

public class InsertAndDelete {
    public static void main(String[] args) {
        int arr[] = {1,2,3,4,5,6,7,8};
        for(int i:arr){
            System.out.print(i + " ");
        }
        System.out.println();
        Delete.Delete(arr,4);
        for(int i:arr){
            System.out.print(i + " ");
        }
        System.out.println();
        Insert.Insert(arr,4);
        for(int i:arr){
            System.out.print(i + " ");
        }
        System.out.println();
    }
}
