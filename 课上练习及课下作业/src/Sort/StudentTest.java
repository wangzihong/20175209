package Sort;

import java.util.*;

public class StudentTest {
    public static void main(String[] args) {
        List<Student> list = new LinkedList<Student>();
        Student a = new Student("20175207", "冷南", '女', 20);
        a.setComputer_score(85);
        a.setMaths_score(88);
        a.setEnglish_score(70);
        Student b = new Student("20175208", "张家华", '男', 20);
        b.setComputer_score(80);
        b.setMaths_score(85);
        b.setEnglish_score(79);
        Student c = new Student("20175210", "闵天", '男', 20);
        c.setComputer_score(86);
        c.setMaths_score(75);
        c.setEnglish_score(84);
        Student d = new Student("20175211", "凌一舟", '男', 20);
        d.setComputer_score(99);
        d.setMaths_score(100);
        d.setEnglish_score(90);
        Student my = new Student("20175209", "王梓鸿", '男', 20);
        my.setComputer_score(90);
        my.setMaths_score(80);
        my.setEnglish_score(80);
        list.add(a);
        list.add(b);
        list.add(c);
        list.add(d);
        list.add(my);
        Iterator<Student> iter = list.iterator();
        System.out.println("排序前的顺序为：");
        while (iter.hasNext()) {
            Student stu = iter.next();
            System.out.println(stu.getId() + " 计算机成绩：" + stu.getComputer_score() +
                    " 数学成绩：" + stu.getMaths_score() + " 英语成绩：" + stu.getEnglish_score() +
                    " 总成绩：" + stu.getTotalScore());
        }
        System.out.println("按照学号升序排序后，链表顺序为：");
        Collections.sort(list, new CompareID());
        iter=list.iterator();
        while (iter.hasNext()) {
            Student stu = iter.next();
            System.out.println(stu.getId() + " 计算机成绩：" + stu.getComputer_score() +
                    " 数学成绩：" + stu.getMaths_score() + " 英语成绩：" + stu.getEnglish_score() +
                    " 总成绩：" + stu.getTotalScore());
        }
        System.out.println("按照总成绩降序排序后，链表顺序为：");
        Collections.sort(list, new CompareScore());
        iter=list.iterator();
        while (iter.hasNext()) {
            Student stu = iter.next();
            System.out.println(stu.getId() + " 计算机成绩：" + stu.getComputer_score() +
                    " 数学成绩：" + stu.getMaths_score() + " 英语成绩：" + stu.getEnglish_score() +
                    " 总成绩：" + stu.getTotalScore());
        }


    }

}

class CompareID implements Comparator<Student> {
    public int compare(Student m,Student n){
        int id1 = Integer.valueOf(m.getId());
        int id2 = Integer.valueOf(n.getId());
        if(id1>id2){
            return 1;
        }
        else{
            return -1;
        }
    }

}

class CompareScore implements Comparator<Student> {
    public int compare(Student m,Student n){
        if(m.getTotalScore()<n.getTotalScore()){
            return 1;
        }
        else {
            return -1;
        }
    }
}