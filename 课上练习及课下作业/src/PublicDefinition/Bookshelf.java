package PublicDefinition;

public class Bookshelf {
    public static void main(String[] args) {
        Book b1 = new Book("Java2实用教程（第五版）","耿祥义","清华大学出版社","2017年1月");
        Book b2 = new Book("密码学","郑秀林","金城出版社","2016年8月");
        Book b3 = new Book("计算机网络","谢希仁","电子工业出版社","2017年1月");
        System.out.println(b1);
        System.out.println(b2);
        System.out.println(b3);
        System.out.println(b1.getDate());
        System.out.println(b2.getPublish());
        System.out.println(b3.getAuthor());
        System.out.println(b1.getName());
        System.out.println(b1.equals(b2));
        System.out.println(b2.equals(b3));
        System.out.println(b2.equals(b2));
    }
}
