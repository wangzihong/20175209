package PublicDefinition;

import java.util.Objects;

public class Book {
    String name;
    String author;
    String publish;
    String date;
    Book(String name,String author) {}
    Book(String name,String author,String publish) {}
    Book(String name,String author,String publish,String date) {
        this.name = name;
        this.author = author;
        this.publish = publish;
        this.date = date;
    }
    public void setName(String name) {

        this.name = name;
    }
    public void setAuthor(String author) {

        this.author = author;
    }
    public void setPublish(String publish) {
        this.publish = publish;
    }
    public void setDate(String date) {

        this.date = date;
    }
    public String getName() {
        return name;
    }
    public String getAuthor() {

        return author;
    }
    public String getPublish() {

        return publish;
    }
    public String getDate() {

        return date;
    }
    public String toString() {

        return "书名为"+name+" 作者是"+author+" 出版社为"+publish+" 出版日期是"+date;
    }
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(obj == null) {
            return false;
        }
        if(getClass() != obj.getClass()) {
            return false;
        }
        if(!(obj instanceof Book)){
            return false;
        }
        else {
            Book book = (Book)obj;
            return Objects.equals(name,book.name)&&Objects.equals(author,book.author)&&Objects.equals(publish,book.publish)&&Objects.equals(date,book.date);
        }
    }
    public int hashCode() {
        return Objects.hash(name,author,publish,date);
    }
}
