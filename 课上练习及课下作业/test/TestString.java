import junit.framework.TestCase;
import org.junit.Test;

public class TestString extends TestCase{
    String str1 = new String("Hello java");
    String str2 = new String("你好，爪哇");
    String s = new String("http://jwc.besti.edu.cn/ wo zhen hao kan");
    @Test
    public void testcharAt() throws Exception {
        assertEquals('l',str1.charAt(3));    //正常情况
        assertEquals(' ',str1.charAt(5));    //正常情况
        assertEquals('好',str2.charAt(1));   //正常情况
        assertEquals(':',s.charAt(4));       //正常情况
        assertEquals('a',str1.charAt(9));    //边界情况
        assertEquals('你',str2.charAt(0));   //边界情况
        //assertEquals(' ',str1.charAt(-1));           //异常情况
        //assertEquals('哇',str2.charAt(5));           //异常情况
    }
    @Test
    public void testsplit() throws Exception {
        assertEquals("http://jwc",s.split("[.]")[0]);    //正常情况
        assertEquals("http://jwc.besti.edu.cn/",s.split(" ")[0]);    //正常情况
        assertEquals("jwc.besti.edu.cn",s.split("/")[2]);    //正常情况
        assertEquals("",s.split("\\p{Lower}")[0]);      //边界情况
        assertEquals("kan",s.split("\\s")[4]);          //边界情况
        //assertEquals("jwc.besti.edu.cn",s.split("/")[1]);              //异常情况
        //assertEquals(" ",s.split(" ")[-1]);                            //异常情况
    }
}
