import junit.framework.TestCase;
import org.junit.Test;

import java.util.Arrays;

public class TestArrays extends TestCase{
    int a[] = {2,5,6,4,9,3};
    char b[] = {'d','y','a','t','z','h'};
    String s[] = {"1","8","4","0","7","3"};
    @Test
    public void testsort() throws Exception {
        Arrays.sort(a);
        assertEquals("[2, 3, 4, 5, 6, 9]",Arrays.toString(a));
        Arrays.sort(b,1,4);
        assertEquals("[d, a, t, y, z, h]",Arrays.toString(b));
        assertEquals('t',b[2]);
        //assertEquals('a',b[0]);           //异常情况
        Arrays.sort(s);
        assertEquals("[0, 1, 3, 4, 7, 8]",Arrays.toString(s));
    }
    @Test
    public void testbinarySearch() throws Exception {
        Arrays.sort(a);
        assertEquals(3,Arrays.binarySearch(a,5));
        //assertEquals(-1,Arrays.binarySearch(a,10));     //异常情况
        //assertEquals(-2,Arrays.binarySearch(a,0));     //异常情况
    }
}