import java.lang.String;
import java.util.Scanner;

public class GetQuestion {
    public String get(int a,int b) {
        String q = "";
        int n=a;
        int flag1 = 0, flag2 = 0,flag4 = 0, flag5 = 0, flag6 = 0, countTotal;  // flag1记录插了几个左括号，flag2标记下一个插入的是数字还是运算符，countTotal记录共插入几个字符
        GetNumber num = new GetNumber();                                       //flag3限制括号数量，flag4控制左右括号之间至少间隔一个运算符
        GetOperator op = new GetOperator();                                     //flag5控制左括号不连续出现
        GetBracket bra = new GetBracket();
        for (countTotal = 0; countTotal < b; ) {
            GetNumber random = new GetNumber();
            int rand = (random.GetNumber(a)) % 2;

            if (countTotal != 0 && rand == 1 && flag2 % 2 == 0 && flag4 % 2 == 0 && flag5 % 2 == 0 &&flag6==0&& countTotal < b-3) {                    //插左括号
                q += bra.setBracket(0);
                flag1++;
                flag5 = 1;
                countTotal++;
            } else if (rand == 0 && flag2 % 2 == 0 || flag5 % 3 == 0 && flag2 % 2 == 0) {                                          //插数字
                if(flag6==1)                                          //当产生了分号时 
                    n = num.GetNumber(a-n)+n;                 //控制分号后面的数比分号前面的数大但不会超过给出的最大值
                else
                    n = num.GetNumber(a);
                q += n;
                flag2++;
                flag5 = 0;
                countTotal++;
            } else if (rand == 1 && flag1 > 0 && flag2 % 2 == 1 && flag4 % 2 == 1) {         //插右括号
                q += bra.setBracket(1);
                flag1--;
                countTotal++;
            } else if (rand == 0 && flag2 % 2 == 1) {                                          //插运算符
                char temp = op.GetOperator();
                q += temp;
                if (temp == '/' && flag6 == 0) {
                    flag6 = 1;
                } else if (temp == '/' && flag6 == 1) {
                    int L = q.length();
                    q = q.substring(0, L - 1);
                    flag2--;
                    flag4--;
                    countTotal--;
                } else flag6 = 0;
                flag2++;
                flag4++;
                countTotal++;
            }
        }
        while (q.endsWith("+") || q.endsWith("-") || q.endsWith("*") || q.endsWith("÷") || q.endsWith("/") || q.endsWith("(") ) {
            int L = q.length();
            if (q.endsWith("("))
                flag1--;

            q = q.substring(0, L - 1);
        }
        while (flag1 > 0) {
            q += bra.setBracket(1);
            flag1--;
        }

        return q;
    }
}

