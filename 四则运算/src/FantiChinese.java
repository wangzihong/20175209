public class FantiChinese extends Language {
    public String getQuestionNumber() {
        return "輸入你想做的題數：";
    }
    public String getMaxNumber() {
        return "請輸入你想要運算的最大數：";
    }
    public String getMaxOperator() {
        return "請輸入你想要運算的算式的長度：";
    }
    public String getInputAnswer() {
        return "輸入你的答案：(分數形式)";
    }
    public String getRight() {
        return "恭喜你，回答正確！";
    }
    public String getWrong() {
        return "回答錯誤！正確答案是";
    }
    public String getRate() {
        return "測試結束，你的正確率為";
    }
    public String getContinue() {
        return "是否想要繼續進行？ 繼續請輸入1,退出請輸入0";
    }
    public String getThanks() {
        return "測試結束，感謝使用！";
    }
}
