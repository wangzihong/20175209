public class English extends Language {
    public String getQuestionNumber() {
        return "Enter the number of questions you want to do:";
    }
    public String getMaxNumber() {
        return "Please enter the maximum number you want to operate:";
    }
    public String getMaxOperator() {
        return "Please enter the length of the expression you want to operate:";
    }
    public String getInputAnswer() {
        return "Enter your answer :(fractal point)";
    }
    public String getRight() {
        return "Congratulations! Your answer is right";
    }
    public String getWrong() {
        return "Wrong answer! The correct answer is:";
    }
    public String getRate() {
        return "Test over! Your accuracy is:";
    }
    public String getContinue() {
        return "Do you want to continue? Please enter 1 to continue and 0 to exit";
    }
    public String getThanks() {
        return "Test over, thanks for using!";
    }
}
