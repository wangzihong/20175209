import java.util.Locale;
import java.util.Scanner;

public class MainSystem {
    public static void main(String[] args) {
        System.out.println("~~~欢迎使用算术测试系统！~~~歡迎使用算術測試系統！~~~Welcome to the arithmetic test system！~~~");
        System.out.println("~~~请选择语言~~~請選擇語言~~~Please choose language~~~(输入1为简体中文，輸入2為繁體中文,Input 3 is Engish)");
        Scanner scanner = new Scanner(System.in);
        Language language;
        int a = scanner.nextInt();
        switch(a) {
            case 1:
                language = new Chinese();
                break;
            case 2:
                language = new FantiChinese();
                break;
            case 3:
                language = new English();
                break;
            default:
                System.out.println("~~~没有这个选项!~~~沒有這個選項!~~~No this choice!~~~默认选择简体中文");
                language = new Chinese();
        }
        int choose=1;
        do {
            System.out.print(language.getQuestionNumber());
            int range = scanner.nextInt();
            SetQuestion question = new SetQuestion();
            question.setQuestion(range,language);
            Scanner b = new Scanner(System.in);
            System.out.println(language.getContinue());
            choose = b.nextInt();
        } while (choose == 1);
        System.out.println(language.getThanks());
    }
}
