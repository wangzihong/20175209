public class GetOperator {
    public char GetOperator() {
        int ch = (int) (Math.random() * 5);
        switch (ch) {
            case 0:
                return '+';
            case 1:
                return '-';
            case 2:
                return '*';
            case 3:
                return '÷';
            case 4:
                return '/';
            default:
                return 'w';
        }
    }
}