import java.lang.*;
import java.util.*;

public class SetQuestion {
    void setQuestion(int range,Language language) {
        String question2 = "";
        int countCorrect=0;
        int maxnum,maxop;
        Scanner in = new Scanner(System.in);
        System.out.print(language.getMaxNumber());
        maxnum = in.nextInt();
        System.out.print(language.getMaxOperator());
        maxop = in.nextInt();
        for (int i = 0; i < range; i++) {
            GetQuestion q = new GetQuestion();
            String question1 = q.get(maxnum,maxop);
            if(question1.equals(question2)) {                     //判断时候和前一题是否相等，相等则不计入次数，重新产生等式
                i--;
                continue;
            }
            System.out.println(question1);
            question2 = question1;                                 //不相同则把该题目赋值给question2用于和下一题进行比较
            System.out.println(language.getInputAnswer());
            Scanner scanner=new Scanner(System.in);
            String answer=scanner.nextLine();
            MidToEnd change = new MidToEnd();
            change.ChangeString(question1);
            question1 = change.ChangeOrder();
            GetAnswer getanswer = new GetAnswer();
            getanswer.set(question1);
            Rational rightanswer = getanswer.get();
            int a = rightanswer.getNumerator();
            int b = rightanswer.getDenominator();
            if(b<0) {
                a = -a;
                b = -b;
            }
            if(answer.equals(a+"/"+b))
            {
                System.out.println(language.getRight());
                countCorrect++;
            }
            else {
                System.out.println(language.getWrong()+a+"/"+b);
            }
        }
        System.out.println(language.getRate()+String.format("%.2f",(float)countCorrect/range*100)+"%");
    }
}
