public class Chinese extends Language {
    public String getQuestionNumber() {
        return "输入你想做的题数：";
    }
    public String getMaxNumber() {
        return "请输入你想要运算的最大数：";
    }
    public String getMaxOperator() {
        return "请输入你想要运算的算式的长度：";
    }
    public String getInputAnswer() {
        return "输入你的答案：(分数形式)";
    }
    public String getRight() {
        return "恭喜你，回答正确！";
    }
    public String getWrong() {
        return "回答错误！正确答案是";
    }
    public String getRate() {
        return "测试结束，你的正确率为";
    }
    public String getContinue() {
        return "是否想要继续进行？ 继续请输入1,退出请输入0";
    }
    public String getThanks() {
        return "测试结束，感谢使用！";
    }
}
