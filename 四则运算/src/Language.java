public abstract class Language {
    public abstract String getQuestionNumber();
    public abstract String getMaxNumber();
    public abstract String getMaxOperator();
    public abstract String getInputAnswer();
    public abstract String getRight();
    public abstract String getWrong();
    public abstract String getRate();
    public abstract String getContinue();
    public abstract String getThanks();
}
