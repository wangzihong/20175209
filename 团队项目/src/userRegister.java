/**
 * @author Jason Tong
 * @date 2019/6/2 10:48.
 */
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javax.swing.*;
public class userRegister implements ActionListener {
    ArrayList<User> list = new ArrayList<User>();
    JTextField jtf1 = new JTextField(14);
    JTextField jtf2 = new JTextField(14);
    JTextField jtf3 = new JTextField(14);
    JTextField jtf4 = new JTextField(14);
    JFrame jf;

    public userRegister() throws Exception {
        createGUI();
    }
    //当事件发生时，自动调用actionPerformed()方法
    @Override
    public void actionPerformed(ActionEvent e) {
        String str = e.getActionCommand();
        if ("注册".equals(str)) {
            User user = new User();
            user.setUserName(jtf1.getText());
            user.setPassWord(jtf2.getText());
            user.setEmail(jtf4.getText());
            String rePassWord = jtf3.getText();
            if (!(user.getPassWord().equalsIgnoreCase(rePassWord))) {
                jtf2.setText("密码输入错误");
                jtf3.setText("密码输入错误");
            } else {
                jf.setTitle("注册成功" + "欢迎您" + user.getUserName());
                jtf2.setText("******");
                jtf3.setText("******");
                list.add(user);
            }
        } else if ("登录".equals(str)) {
            try {
                readFromFile();
            } catch (Exception e1) {
                e1.printStackTrace();}}}
    //createGui()方法，用于生成图形用户界面
    public void createGUI() {
        jf = new JFrame("用户注册");
//当用户关闭窗口时，多个用户信息自动保存到本地文件系统(通过调用writeToFile()方法)
        jf.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    writeToFile();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                System.exit(0);
            }
        });
        jf.setLayout(new GridLayout(4, 2));
        JPanel jp1 = new JPanel();
        jf.add(jp1);
        JLabel jl1 = new JLabel("User Name:");
        jp1.add(jl1);jp1.add(jtf1);
        JPanel jp2 = new JPanel();
        jf.add(jp2);
        JLabel jl2 = new JLabel("Pass Word:");
        jp2.add(jl2);jp2.add(jtf2);
        JPanel jp3 = new JPanel();
        jf.add(jp3);
        JLabel jl3 = new JLabel("ReInputPwd:");
        jp3.add(jl3);
        jp3.add(jtf3);
        JPanel jp4 = new JPanel();
        jf.add(jp4);
        JLabel jl4 = new JLabel("E-mail:");
        jp4.add(jl4);jp4.add(jtf4);
        JPanel jp5 = new JPanel();
        jf.add(jp5);
        JButton jb51 = new JButton("注册");
        jb51.addActionListener(this);
        JButton jb52 = new JButton("登录");
        jb52.addActionListener(this);
        jp5.add(jb51);jp5.add(jb52);
        jf.setSize(500, 300);
        jf.setVisible(true);
    }

    //readFromFile()方法用于从文件中读取用户对象信息，并将用户对象添加进线性表（list)
    public void readFromFile() throws IOException, Exception {
        FileInputStream fis = new FileInputStream(new File(
                "C:\\Users\\wangzihong\\Desktop\\20175209\\团队项目\\userRegister.dat"));
        ObjectInputStream ois = new ObjectInputStream(fis);
        User read = null;
        try {
            while (true) {
                read = (User) ois.readObject();
                if (read == null) {
                    break;
                }
                list.add(read);
            }
        } catch (Exception e) {
        }
// System.out.println(list);
        User temp = new User(jtf1.getText(), jtf2.getText(), "q");
        match(temp);
        ois.close();
    }
    //writeToFile()方法，实现用户信息保存功能。
    public void writeToFile() throws IOException {
        File file = new File("C:\\Users\\wangzihong\\Desktop\\20175209\\团队项目\\userRegister.dat");
        file.createNewFile();
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(
                file));
        for (User usr : list) {
            oos.writeObject(usr);
        }
        oos.flush();
        oos.close();
    }

    //当用户试图登录时，match()方法可以将用户输入与保存的信息，进行匹配。
//如果用户名和密码正确，登录成功；否则，登录失败。
    public void match(User user) {
        boolean match = false;
        for (User usr : list) {
            if (usr.getUserName().equals(user.getUserName())
                    && usr.getPassWord().equals(user.getPassWord())) {
                jf.setTitle("登录成功");
                match = true;
                WindowButton win = new WindowButton("黄金点游戏");
            }
        }
        if (!match) {
            jf.setTitle("用户名或密码错误，请重新输入！");
            jtf1.setText("");
            jtf2.setText("");
            jtf3.setText("");
            jtf4.setText("");
            jtf1.requestFocus();
        }
    }

    public static void main(String[] args) throws Exception {
        new userRegister();
    }
}

//------------------------------------------------------------//

