package ch6;

public class lianxi6 {
    public static void main(String[] args) {
        Simulator6 simulator = new Simulator6();
        simulator.playSound(new Dog_6());
        simulator.playSound(new Cat_6());
    }

}

interface Animal6 {
    void cry();
    String getAnimalName();
}

class Simulator6 {
     public void playSound(Animal6 animal) {
         System.out.println("播放"+animal.getAnimalName()+"的声音");
         animal.cry();
    }
}

class Dog_6 implements Animal6 {
    public void cry() {
        System.out.println("汪汪汪");
    }
    public String getAnimalName() {
        return "狗";
    }
}

class Cat_6 implements Animal6 {
    public void cry() {
        System.out.println("喵喵喵");
    }
    public String getAnimalName() {
        return "猫";
    }
}