package ch8;

import java.util.*;

public class Exercise8_6 {
    public static void main(String[] args) {
        String cost = "数学87分，物理76分，英语96分";
        Scanner scanner = new Scanner(cost);
        scanner.useDelimiter("[^0123456789.]+");
        double sum = 0;
        int count=0;
        while(scanner.hasNext()) {
            try {
                count++;
                double score = scanner.nextDouble();
                sum = sum + score;
            }
            catch(InputMismatchException exp) {
                String t = scanner.next();
            }
        }
        System.out.println("总成绩为："+sum);
        System.out.println("平均分为："+sum/count);
    }
}
