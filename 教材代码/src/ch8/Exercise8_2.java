package ch8;

public class Exercise8_2 {
    public static void main(String[] args) {
        String s = "1qaz@wsx#";
        char firstchar = s.charAt(0);
        char lastchar = s.charAt(s.length()-1);
        System.out.println(firstchar);
        System.out.println(lastchar);
    }

}
