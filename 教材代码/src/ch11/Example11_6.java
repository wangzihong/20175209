package ch11;

import javax.swing.*;
import java.sql.*;

public class Example11_6 {
    public static void main(String args[]) {
        String [] tableHead;
        String [][] content;
        JTable table ;
        JFrame win= new JFrame();
        Query findRecord = new  Query();
        findRecord.setDatabaseName("students");
        findRecord.setSQL("select * from mess");
        content = findRecord.getRecord();
        tableHead=findRecord.getColumnName();
        table = new JTable(content,tableHead);
        win.add(new JScrollPane(table));
        win.setBounds(12,100,400,200);
        win.setVisible(true);
        win.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}

class Query {
    String databaseName="";
    String SQL;
    String [] columnName;
    String [][] record;
    public Query() {
        try{  Class.forName("com.mysql.cj.jdbc.Driver");//加载JDBC-MySQL驱动
        }
        catch(Exception e){}
    }
    public void setDatabaseName(String s) {
        databaseName=s.trim();
    }
    public void setSQL(String SQL) {
        this.SQL=SQL.trim();
    }
    public String[] getColumnName() {
        if(columnName ==null ){
            System.out.println("先查询记录");
            return null;
        }
        return columnName;
    }
    public String[][] getRecord() {
        startQuery();
        return record;
    }
    private void startQuery() {
        Connection con;
        Statement sql;
        ResultSet rs;
        String uri =
                "jdbc:mysql://localhost:3306/"+
                        databaseName+"?serverTimezone=GMT%2B8&characterEncoding=utf-8";
        try {
            con=DriverManager.getConnection(uri,"root","wzh990213");
            sql=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            rs=sql.executeQuery(SQL);
            ResultSetMetaData metaData = rs.getMetaData();
            int columnCount = metaData.getColumnCount();//字段数目
            columnName=new String[columnCount];
            for(int i=1;i<=columnCount;i++){
                columnName[i-1]=metaData.getColumnName(i);
            }
            rs.last();
            int recordAmount =rs.getRow();  //结果集中的记录数目
            record = new String[recordAmount][columnCount];
            int i=0;
            rs.beforeFirst();
            while(rs.next()) {
                for(int j=1;j<=columnCount;j++){
                    record[i][j-1]=rs.getString(j); //第i条记录，放入二维数组的第i行
                }
                i++;
            }
            con.close();
        }
        catch(SQLException e) {
            System.out.println("请输入正确的表名"+e);
        }
    }
}