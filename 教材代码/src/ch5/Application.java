package ch5;

public class Application {
    public static void main(String[] args) {
        Pillar pillar;
        Geometry bottom = null;
        pillar = new Pillar (bottom,10);
        System.out.println("体积"+pillar.getVolume());
        bottom=new Rectangle(12,22);
        pillar =new Pillar (bottom,58);
        System.out.println("体积"+pillar.getVolume());
        bottom=new Cricle(10);
        pillar =new Pillar (bottom,58);
        System.out.println("体积"+pillar.getVolume());
    }
}

abstract class Geometry {
    public abstract double getArea();
}

class Pillar {
    Geometry bottom;
    double height;
    Pillar (Geometry bottom,double height) {
        this.bottom=bottom;
        this.height=height;
    }
    public double getVolume() {
        if(bottom== null) {
            System.out.println("没有底，无法计算体积");
            return -1;
        }
        return bottom.getArea()*height;
    }
}

class Cricle extends Geometry {
    double r;
    Cricle(double r) {
        this.r=r;
    }
    public double getArea() {
        return (3.14*r*r);
    }
}

class Rectangle extends Geometry {
    double a,b;
    Rectangle(double a,double b) {
        this.a = a;
        this.b = b;
    }
    public double getArea() {
        return a*b;
    }
}