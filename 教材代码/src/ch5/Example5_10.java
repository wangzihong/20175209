package ch5;

public class Example5_10 {
    public static void main(String args[]) {
        类人猿 monkey;
        People5_10 geng = new People5_10();
        monkey = geng ;
        monkey.crySpeak("I love this game");
        geng.crySpeak("I love this game");
        People5_10 people=(People5_10)monkey; //把上转型对象强制转化为子类的对象
        people.computer(10,10);
    }
}

class  类人猿 {
    void crySpeak(String s) {
        System.out.println(s);
    }
}


class People5_10 extends 类人猿 {
    void computer(int a,int b) {
        int c=a*b;
        System.out.println(c);
    }
    void crySpeak(String s) {
        System.out.println("***"+s+"***");
    }
}

