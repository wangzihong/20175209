package ch5;

public class Exercise5_1 {
    public static void main(String[] args) {
        Simulator simulator = new Simulator();
        Animal animal = new Dog();
        simulator.playSound(animal);
        animal = new Cat();
        simulator.playSound(animal);
    }
}

abstract class Animal {
    public abstract void cry();
    public abstract String getAnimalName();
}

class Simulator {
    public void playSound(Animal animal) {
        System.out.print("播放"+animal.getAnimalName()+"的声音");
        animal.cry();
    }
}

class Dog extends Animal {
    public String getAnimalName() {
        ;return "狗";
    }
    public void cry() {
        System.out.println("汪汪");
    }
}

class Cat extends Animal {
    public String getAnimalName() {
        ;return "猫";
    }
    public void cry() {
        System.out.println("喵喵");
    }
}