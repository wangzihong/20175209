package ch5;

public class Example5_6 {
    public static void main(String args[]) {
        B5_6 b=new B5_6();
        Integer t=b.get();
        System.out.println(t.intValue());
    }
}

class A5_6 {
    Object get() {
        return null;
    }
}

class B5_6 extends A5_6 {
    Integer get() {
        return new Integer(100);
    }
}