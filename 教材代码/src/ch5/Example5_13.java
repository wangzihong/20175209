package ch5;

public class Example5_13 {
    public static void main(String args[]) {
        MobileTelephone5_13 telephone = new MobileTelephone5_13 ();
        SIM5_13 sim=new SIMOfChinaMobile();
        sim.setNumber("13887656432");
        telephone.useSIM(sim);
        telephone.showMess();
        sim=new SIMOfChinaUnicom();
        sim.setNumber("13097656437");
        telephone.useSIM(sim);
        telephone.showMess();
    }
}

abstract class SIM5_13 {
    public abstract void setNumber(String n);
    public abstract String giveNumber();
    public abstract String giveCorpName();
}

class MobileTelephone5_13 {
    SIM5_13 card;
    public void useSIM(SIM5_13 card) {
        this.card=card;
    }
    public void showMess() {
        System.out.println("使用的卡是:"+card.giveCorpName()+"提供的");
        System.out.println("手机号码是:"+card.giveNumber());
    }
}

class SIMOfChinaMobile extends SIM5_13 {
    String number;
    public void setNumber(String n) {
        number = n;
    }
    public String giveNumber() {
        return number;
    }
    public String giveCorpName() {
        return "中国移动";
    }
}

class SIMOfChinaUnicom extends SIM5_13 {
    String number;
    public void setNumber(String n) {
        number = n;
    }
    public String giveNumber() {
        return number;
    }
    public String giveCorpName() {
        return "中国联通";
    }
}