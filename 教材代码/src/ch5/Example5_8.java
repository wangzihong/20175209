package ch5;

public class Example5_8 {
    public static void main(String args[]) {
        UniverStudent5_8 wzh =new UniverStudent5_8(20175209,"王梓鸿",false);
        UniverStudent5_8 lzh =new UniverStudent5_8(20175213,"吕正宏",false);
    }
}

class Student5_8 {
    int number;String name;
    Student5_8() {
    }
    Student5_8(int number,String name) {
        this.number=number;
        this.name=name;
        System.out.println("我的名字是:"+name+ "学号是:"+number);
    }
}


class UniverStudent5_8 extends Student5_8 {
    boolean 婚否;
    UniverStudent5_8(int number,String name,boolean b) {
        super(number,name);
        婚否=b;
        System.out.println("婚否="+婚否);
    }
}


