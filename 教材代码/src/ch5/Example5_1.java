package ch5;

public class Example5_1 {
    public static void main(String[] args) {
        Student5_1 zhang = new Student5_1();
        zhang.age = 17;
        zhang.number=100101;
        zhang.showPeopleMess();
        zhang.tellNumber();
        int x=9,y=29;
        System.out.print("会做加法：");
        int result=zhang.add(x,y);
        System.out.printf("%d+%d=%d\n",x,y,result);
        UniverStudent geng = new UniverStudent();
        geng.age = 21;
        geng.number=6609;
        geng.showPeopleMess();
        geng.tellNumber();
        System.out.print("会做加法：");
        result = geng.add(x,y);
        System.out.printf("%d+%d=%d\t",x,y,result);
        System.out.print("会做乘法：");
        result=geng.multi(x,y);
        System.out.printf("%d×%d=%d\n",x,y,result);
    }
}

class People5_1 {
    int age,leg=2,hand=2;
    protected void showPeopleMess() {
        System.out.printf("%d岁,%d只脚,%d只手\t",age,leg,hand);
    }
 }

 class Student5_1 extends People5_1  {
    int number;
    void tellNumber() {
        System.out.printf("学号:%d\t",number);
    }
    int add(int x,int y) {
        return x+y;
    }
 }

 class UniverStudent extends Student5_1 {
    int multi(int x,int y) {
        return x*y;
    }
 }