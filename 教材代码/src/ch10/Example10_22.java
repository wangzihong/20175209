package ch10;

import java.io.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Example10_22 {
    public static void main(String args[]) {
        JDKWindow win=new JDKWindow();
    }
}

class CompileDialog  extends JDialog {
    JTextArea showError;
    CompileDialog() {
        setTitle("编译对话框");
        showError = new JTextArea();
        Font f =new Font("宋体",Font.BOLD,15);
        showError.setFont(f);
        add(new JScrollPane(showError),BorderLayout.CENTER);
        setBounds(10,10,500,300);
    }
    public void compile(String name) {
        try{  Runtime ce=Runtime.getRuntime();
            Process proccess = ce.exec("javac "+name);
            InputStream in=proccess.getErrorStream();
            BufferedInputStream bin=new BufferedInputStream(in);
            int n;
            boolean bn=true;
            byte error[]=new byte[100];
            while((n=bin.read(error,0,100))!=-1) {
                String s=null;
                s=new String(error,0,n);
                showError.append(s);
                if(s!=null) bn=false;
            }
            if(bn)  showError.append("编译正确");
        }
        catch(IOException e1){}
    }
}

class JDKWindow extends JFrame {
    JTextField javaSourceFileName; //输入Java源文件
    JTextField javaMainClassName;  //输入主类名
    JButton compile,run,edit;
    HandleActionEvent listener;
    public JDKWindow(){
        edit = new JButton("用记事本编辑源文件");
        compile = new JButton("编译");
        run = new JButton("运行");
        javaSourceFileName = new JTextField(10);
        javaMainClassName = new JTextField(10);
        setLayout(new FlowLayout());
        add(edit);
        add(new JLabel("输入源文件名:"));
        add(javaSourceFileName);
        add(compile);
        add(new JLabel("输入主类名:"));
        add(javaMainClassName);
        add(run);
        listener = new HandleActionEvent();
        edit.addActionListener(listener);
        compile.addActionListener(listener);
        run.addActionListener(listener);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100,100,750,180);
    }
    class  HandleActionEvent implements ActionListener { //内部类实例做监视器
        public void actionPerformed(ActionEvent e) {
            if(e.getSource()==edit) {
                Runtime ce=Runtime.getRuntime();
                String name = javaSourceFileName.getText();
                File file=new File("c:/windows","Notepad.exe "+name);
                try{
                    ce.exec(file.getAbsolutePath());
                }
                catch(Exception exp){}
            }
            else if(e.getSource()==compile) {
                CompileDialog compileDialog = new CompileDialog();
                String name = javaSourceFileName.getText();
                compileDialog.compile(name);
                compileDialog.setVisible(true);
            }
            else if(e.getSource()==run) {
                RunDialog runDialog =new RunDialog();
                String name = javaMainClassName.getText();
                runDialog.run(name);
                runDialog.setVisible(true);
            }
        }
    }
}

class RunDialog  extends JDialog {
    JTextArea showOut;
    RunDialog() {
        setTitle("运行对话框");
        showOut = new JTextArea();
        Font f =new Font("宋体",Font.BOLD,15);
        showOut.setFont(f);
        add(new JScrollPane(showOut),BorderLayout.CENTER);
        setBounds(210,10,500,300);
    }
    public void run(String name) {
        try{  Runtime ce=Runtime.getRuntime();
            Process proccess = ce.exec("java "+name);
            InputStream in=proccess.getInputStream();
            BufferedInputStream bin=new BufferedInputStream(in);
            int n;
            boolean bn=true;
            byte mess[]=new byte[100];
            while((n=bin.read(mess,0,100))!=-1) {
                String s=null;
                s=new String(mess,0,n);
                showOut.append(s);
                if(s!=null) bn = false;
                if(bn)  showOut.setText("Java程序中没使用out流输出信息");
            }
        }
        catch(IOException e1){}
    }
}