package ch7;

public class Example7_7 {
    public static void main(String args[]) {
        CargoBoat ship = new CargoBoat();
        ship.setMaxContent(1000);
        int m =200;
        try{
            ship.loading1(m);
            ship.loading(m);
            m = 400;
            ship.loading1(m);
            ship.loading(m);
            m = 367;
            ship.loading1(m);
            ship.loading(m);
            m = 555;
            ship.loading1(m);
            ship.loading(m);
        }
        catch(DangerException e) {
            System.out.println(e.warnMess());
            System.out.println("无法再装载重量是"+m+"吨的集装箱");
        }
        catch(DangerException2 e) {
            System.out.println(e.warnMess());
            System.out.println("无法再装载重量是"+m+"吨的集装箱");
        }
        finally {
            System.out.printf("货船将正点启航");
        }
    }
}

class CargoBoat {
    int realContent;
    int maxContent;
    int minContent = 209;
    public void setMaxContent(int c) {
        maxContent = c;
    }
    public void loading(int m) throws DangerException {
        realContent += m;
        if(realContent>maxContent) {
            throw new DangerException();
        }
        System.out.println("目前装载了"+realContent+"吨货物");
    }
    public void loading1(int m) throws DangerException2 {
        realContent += m;
        if(realContent<209) {
            throw new DangerException2();
        }
        System.out.println("目前装载了"+realContent+"吨货物");
    }
}

class DangerException extends Exception {
    final String message = "超重";
    public String warnMess() {
        return message;
    }
}

class DangerException2 extends Exception {
    final String message = "超轻";
    public String warnMess() {
        return message;
    }
}