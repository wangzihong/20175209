package ch4;

public class Example4_13 {
    public static void main(String args[]) {
        Circle1 circle = new Circle1();
        circle.setRadius(196.87);
        Tixing lader = new Tixing(3,21,9);
        Student2 zhang = new Student2();
        System.out.println("zhang计算圆的面积：");
        double result=zhang.computerArea(circle);
        System.out.println(result);
        System.out.println("zhang计算梯形的面积：");
        result=zhang.computerArea(lader);
        System.out.println(result);
    }
}

class Circle1 {
    double radius,area;
    void setRadius(double r) {
        radius=r;
    }
    double getArea(){
        area=3.14*radius*radius;
        return area;
    }
}

class Student2 {
    double computerArea(Circle1 c) {
        double area=c.getArea();
        return area;
    }
    double computerArea(Tixing t) {
        double area=t.getArea();
        return area;
    }
}

class Tixing {
    double above,bottom,height;
    Tixing(double a,double b,double h) {
        above = a;
        bottom = b;
        height = h;
    }
    double getArea() {
        return (above+bottom)*height/2;
    }
}
