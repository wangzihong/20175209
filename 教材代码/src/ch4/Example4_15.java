package ch4;

public class Example4_15 {
    public static void main(String args[]){
        Student stu=new Student(10201);
        stu.speak();
        System.out.println("主类的包名也是tom.jiafei");
    }
}

class Student{
    int number;
    Student(int n){
        number=n;
    }
    void speak(){
        System.out.println("Student类的包名是tom.jiafei,我的学号："+number);
    }
}
