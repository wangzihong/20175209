package ch4;

public class Test {
    public static void main(String[] args) {
        CPU cpu = new CPU();
        CPU cpu1 = new CPU();
        cpu.speed = 2200;
        cpu1.speed = 2500;
        HardDisk HD = new HardDisk();
        HardDisk HD1 = new HardDisk();
        HD.amount = 200;
        HD1.amount = 300;
        PC pc = new PC();
        PC pc1 = new PC();
        pc.setCPU(cpu);
        pc.setHardDisk(HD);
        pc.show();
        pc1.setCPU(cpu1);
        pc1.setHardDisk(HD1);
        pc1.show();
        System.out.println("pc1.toString()="+pc1.toString());
        System.out.println("cpu1.toString()="+cpu1.toString());
        System.out.println("HD1.toString()="+HD1.toString());
        System.out.println("PC.equals(pc,pc1)="+PC.equals(pc,pc1));
        System.out.println("CPU.equals(cpu,cpu1)="+CPU.equals(cpu,cpu1));
        System.out.println("HardDisk.equals(HD,HD1)="+HardDisk.equals(HD,HD1));
    }
}

class CPU {
    int speed;
    int getspeed() {
        return speed;
    }
    public String toString() {
        return Integer.toString(speed);
    }
    static boolean equals(CPU a, CPU b) {
        if (a.speed == b.speed)
            return true;
        else
            return false;
    }
}

class HardDisk {
    int amount;
    int getAmount() {
        return amount;
    }
    public void setAmount(int amount) {
        this.amount = amount;
    }
    public String toString() {
        return Integer.toString(amount);
    }
    static boolean equals(HardDisk a,HardDisk b) {
        if(a.amount==b.amount)
            return true;
        else
            return false;
    }
}

class PC {
    CPU cpu;
    HardDisk HD;
    void setCPU(CPU cpu) {
        this.cpu = cpu;
    }
    void setHardDisk(HardDisk HD) {
        this.HD = HD;
    }
    void show() {
        System.out.println("CPU速度"+cpu.getspeed());
        System.out.println("硬盘容量"+HD.getAmount());
    }
    public String toString() {
        String s = new String("CPU speed:");
        s = s+Integer.toString(cpu.speed);
        s = s+"  HardDisk amount:";
        s = s+Integer.toString(HD.amount);
        return s;
    }
    static boolean equals(PC a,PC b) {
        if((a.cpu.speed==b.cpu.speed)&&(a.HD.amount==b.HD.amount))
            return true;
        else
            return false;
    }
    PC(){
    }
    PC(CPU cpu){
        this.cpu = cpu;
    }
    PC(HardDisk HD){
        this.HD = HD;
    }
    PC(CPU cpu, HardDisk HD){
        this.cpu = cpu;
        this.HD = HD;
    }
}