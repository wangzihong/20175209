package ch4;

public class Example4_10 {
    public static void main(String args[]) {
        Lader1.下底=100;     //Lader的字节码被加载到内存,通过类名操作类变量
        Lader1 laderOne=new Lader1();
        Lader1 laderTwo=new Lader1();
        laderOne.设置上底(28);
        laderTwo.设置上底(66);
        System.out.println("laderOne的上底:"+laderOne.获取上底());
        System.out.println("laderOne的下底:"+laderOne.获取下底());
        System.out.println("laderTwo的上底:"+laderTwo.获取上底());
        System.out.println("laderTwo的下底:"+laderTwo.获取下底());
    }
}

class Lader1 {
    double 上底,高;       //实例变量
    static double 下底;     //类变量
    void 设置上底(double a) {
        上底 = a;
    }
    void 设置下底(double b) {
        下底 = b;
    }
    double 获取上底() {
        return 上底;
    }
    double 获取下底() {
        return 下底;
    }
}