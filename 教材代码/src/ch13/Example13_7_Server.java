package ch13;

import java.io.*;
import java.net.*;
import java.sql.*;

public class Example13_7_Server {
    public static void main(String args[]) {
        Connection con;
        PreparedStatement sqlOne=null,sqlTwo=null;
        ResultSet rs;
        try{  Class.forName("com.mysql.cj.jdbc.Driver");
        }
        catch(ClassNotFoundException e){}
        try{  con= GetDBConnection.connectDB("students","root","wzh990213");
            sqlOne=con.prepareStatement("SELECT * FROM mess WHERE number=? ");
            sqlTwo=con.prepareStatement("SELECT * FROM mess WHERE name =?");
        }
        catch(SQLException e){}
        ServerSocket server=null;
        Runnable target;
        Thread threadForClient = null;
        Socket socketOnServer = null;
        while(true) {
            try{  server=new ServerSocket(4331);
            }
            catch(IOException e1) {
                System.out.println("正在监听");
            }
            try{ System.out.println(" 等待客户呼叫");
                socketOnServer = server.accept();
                System.out.println("客户的地址:"+
                        socketOnServer.getInetAddress());
            }
            catch(IOException e) {
                System.out.println("正在等待客户");
            }
            if(socketOnServer!=null) {
                target = new Target(socketOnServer,sqlOne,sqlTwo);
                threadForClient =new Thread(target);
                threadForClient.start();
            }
        }
    }
}

class GetDBConnection {
    public static Connection connectDB(String DBName,String id,String p) {
        Connection con = null;
        String
                uri = "jdbc:mysql://localhost:3306/"+DBName+"?serverTimezone=GMT%2B8&characterEncoding=utf-8";
        try{  Class.forName("com.mysql.cj.jdbc.Driver");//加载JDBC-MySQL驱动
        }
        catch(Exception e){}
        try{
            con = DriverManager.getConnection(uri,id,p); //连接代码
        }
        catch(SQLException e){}
        return con;
    }
}

class Target extends Thread { //implements Runnable {
    Socket socket;
    DataOutputStream out=null;
    DataInputStream  in=null;
    PreparedStatement sqlOne,sqlTwo;
    boolean boo=false;
    Target(Socket t, PreparedStatement sqlOne,PreparedStatement sqlTwo) {
        socket=t;
        this.sqlOne=sqlOne;
        this.sqlTwo=sqlTwo;
        try {  out=new DataOutputStream(socket.getOutputStream());
            in=new DataInputStream(socket.getInputStream());
        }
        catch(IOException e){
            System.out.println(e);
        }
    }
    public void run() {
        ResultSet rs = null;
        while(true) {
            try{
                String str=in.readUTF();
                if(str.startsWith("number:")) {
                    str = str.substring(str.indexOf(":")+1);
                    sqlOne.setString(1,str);
                    rs=sqlOne.executeQuery();
                }
                else if(str.startsWith("name")) {
                    str = str.substring(str.indexOf(":")+1);
                    sqlTwo.setString(1,str);
                    rs=sqlTwo.executeQuery();
                }
                while(rs.next()) {
                    boo=true;
                    String number=rs.getString(1);
                    String name=rs.getString(2);
                    Date time=rs.getDate(3);
                    float height=rs.getFloat(4);
                    out.writeUTF("学号:"+number+"姓名:"+name+"出生日期:"+ time+
                            "身高:"+height);
                }
                if(boo==false)
                    out.writeUTF("没该学号！");
            }
            catch (IOException e) {
                System.out.println("客户离开"+e);
                return;
            }
            catch (SQLException e) {
                System.out.println(e);
            }
        }
    }
}
