package ch12;

public class Example12_4 {
    public static void main(String args[ ]) {
        House12_4 house = new House12_4();
        house.setWater(10);
        house.dog.start();
        house.cat.start();
    }
}

class House12_4 implements Runnable {
    int waterAmount;
    Thread dog,cat;
    House12_4() {
        dog=new Thread(this);
        cat=new Thread(this);
    }
    public void setWater(int w) {
        waterAmount = w;
    }
    public void run() {
        while(true) {
            Thread t=Thread.currentThread();
            if(t==dog) {
                System.out.println("家狗喝水") ;
                waterAmount=waterAmount-2;
            }
            else if(t==cat){
                System.out.println("家猫喝水") ;
                waterAmount=waterAmount-1;
            }
            System.out.println(" 剩 "+waterAmount);
            try{  Thread.sleep(2000);
            }
            catch(InterruptedException e){}
            if(waterAmount<=0) {
                return;
            }
        }
    }
}
