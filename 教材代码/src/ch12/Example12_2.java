package ch12;

public class Example12_2 {
    public static void main(String args[]) {
        Thread speakElephant;
        Thread speakCar;
        ElephantTarget elephant;
        CarTarget car;
        elephant = new ElephantTarget();
        car = new CarTarget();
        speakElephant = new Thread(elephant);
        speakCar = new Thread(car);
        speakElephant.start();
        speakCar.start();
        for(int i=1;i<=15;i++) {
            System.out.print("主人"+i+"  ");
        }
    }
}

class ElephantTarget implements Runnable {
    public void run() {
        for(int i=1;i<=20;i++) {
            System.out.print("大象"+i+"  ");
        }
    }
}

class CarTarget implements Runnable {
    public void run() {
        for(int i=1;i<=20;i++) {
            System.out.print("轿车"+i+"  ");
        }
    }
}
