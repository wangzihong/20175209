package ch12;

public class Example12_1 {
    public  static void main(String args[]) { //主线程
        SpeakElephant  speakElephant;
        SpeakCar  speakCar;
        speakElephant = new SpeakElephant() ;
        speakCar = new SpeakCar();
        speakElephant.start();
        speakCar.start();
        for(int i=1;i<=15;i++) {
            System.out.print("主人"+i+"  ");
        }
    }
}

class SpeakElephant extends Thread {
    public void run() {
        for(int i=1;i<=20;i++) {
            System.out.print("大象"+i+"  ");
        }
    }
}

class SpeakCar extends Thread {
    public void run() {
        for(int i=1;i<=20;i++) {
            System.out.print("轿车"+i+"  ");
        }
    }
}