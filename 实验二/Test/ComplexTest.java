import junit.framework.TestCase;
import org.junit.Test;

public class ComplexTest extends TestCase {
    Complex a = new Complex(2.0,5.0);
    Complex b = new Complex(1.0,-4.0);
    Complex c = new Complex(-3.0,2.0);
    Complex d = new Complex(-4.0,-3.0);
    Complex e = new Complex(0.0,0.0);
    @Test
    public void testgetRealPart() throws Exception {
        assertEquals(2.0,a.getRealPart());
        assertEquals(1.0,b.getRealPart());
        assertEquals(-3.0,c.getRealPart());
        assertEquals(-4.0,d.getRealPart());
        assertEquals(0.0,e.getRealPart());
    }
    @Test
    public void testgetImagePart() throws Exception {
        assertEquals(5.0,a.getImagePart());
        assertEquals(-4.0,b.getImagePart());
        assertEquals(2.0,c.getImagePart());
        assertEquals(-3.0,d.getImagePart());
        assertEquals(0.0,e.getImagePart());
    }
    @Test
    public void testequals() throws Exception {
        assertEquals(true,a.toString().equals(a.toString()));
        assertEquals(false,b.toString().equals(c.toString()));
        assertEquals(false,d.toString().equals(e.toString()));
    }
    @Test
    public void testtoString() throws Exception {
        assertEquals("2.0+5.0i",a.toString());
        assertEquals("1.0-4.0i",b.toString());
        assertEquals("-3.0+2.0i",c.toString());
        assertEquals("-4.0-3.0i",d.toString());
        assertEquals("0.0",e.toString());
    }
    @Test
    public void testComplexAdd() throws Exception {
        assertEquals("3.0+1.0i",a.ComplexAdd(b).toString());
        assertEquals("-7.0-1.0i",c.ComplexAdd(d).toString());
        assertEquals("-4.0-3.0i",d.ComplexAdd(e).toString());
    }
    @Test
    public void testComplexSub() throws Exception {
        assertEquals("1.0+9.0i",a.ComplexSub(b).toString());
        assertEquals("1.0+5.0i",c.ComplexSub(d).toString());
        assertEquals("-3.0+2.0i",c.ComplexSub(e).toString());
    }
    public void testComplexMulti() throws Exception {
        assertEquals("22.0-3.0i",a.ComplexMulti(b).toString());
        assertEquals("18.0+1.0i",c.ComplexMulti(d).toString());
        assertEquals("0.0",b.ComplexMulti(e).toString());
    }
    public void testComplexDiv() throws Exception {
        assertEquals("-1.4666666666666666+0.2i",a.ComplexDiv(b).toString());
        assertEquals("2.5714285714285716+0.14285714285714285i",c.ComplexDiv(d).toString());
        assertEquals("0.0",a.ComplexDiv(e).toString());
    }
}