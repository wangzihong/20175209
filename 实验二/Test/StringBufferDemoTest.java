import junit.framework.TestCase;
import org.junit.Test;

public class StringBufferDemoTest extends TestCase {
    StringBuffer a = new StringBuffer("StringBuffer");
    StringBuffer b = new StringBuffer("StringBufferStringBuffer");
    StringBuffer c = new StringBuffer("StringBuffer12345StringBuffer12345");
    @Test
    public void testcharAt() throws Exception {
        assertEquals('S',a.charAt(0));
        assertEquals('u',b.charAt(7));
        assertEquals('g',c.charAt(22));
    }
    @Test
    public void testcapacity() throws  Exception {
        assertEquals(28,a.capacity());
        assertEquals(40,b.capacity());
        assertEquals(50,c.capacity());
    }
    @Test
    public void testlength() throws  Exception {
        assertEquals(12,a.length());
        assertEquals(24,b.length());
        assertEquals(34,c.length());
    }
    @Test
    public void testindexOf() throws Exception {
        assertEquals(6,a.indexOf("Buf"));
        assertEquals(11,b.indexOf("rSt"));
        assertEquals(15,c.indexOf("45St"));
    }
}