public class StringBufferDemo {
    StringBuffer buffer = new StringBuffer();
    public void StringBufferDemo (StringBuffer buffer) {
        this.buffer = buffer;
    }
    public Character charAt(int i) {
        return buffer.charAt(i);
    }
    public int capacity() {
        return buffer.capacity();
    }
    public int length() {
        return buffer.length();
    }
    public int indexOf(String s) {
        return buffer.indexOf(s);
    }
}
