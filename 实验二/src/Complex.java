public class Complex {
    double RealPart;
    double ImagePart;
    public Complex() {}
    public Complex(double R,double I) {
        RealPart = R;
        ImagePart = I;
    }
    public double getRealPart() {
        return RealPart;
    }
    public double getImagePart() {
        return ImagePart;
    }
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        if(!(obj instanceof Complex))
            return false;
        else {
            Complex complex = (Complex) obj;
            if (complex.ImagePart == ((Complex) obj).ImagePart)
                return true;
            if (complex.RealPart == ((Complex) obj).RealPart)
                return true;
        }
        return false;
    }
    public  String toString() {
        String str = "";
        if (RealPart == 0&&ImagePart == 0)
            str = "0.0";
        else if (RealPart == 0&&ImagePart != 0)
            str = ImagePart + "" + "i";
        else if (RealPart != 0&&ImagePart == 0)
            str = RealPart + "";
        else if (RealPart !=0&&ImagePart > 0)
            str = RealPart + "" + "+" + ImagePart + "i";
        else if (RealPart !=0&&ImagePart < 0)
            str = RealPart + "" + ImagePart + "i";
        return str;
    }
    public Complex ComplexAdd(Complex a) {
        return new Complex(RealPart + a.getRealPart(),ImagePart + a.getImagePart());
    }
    public  Complex ComplexSub(Complex a) {
        return new Complex(RealPart - a.getRealPart(),ImagePart - a.getImagePart());
    }
    public Complex ComplexMulti(Complex a) {
        return new Complex(RealPart*a.getRealPart() - ImagePart*a.getImagePart(),RealPart*a.getImagePart() + ImagePart*a.getRealPart());
    }
    public Complex ComplexDiv(Complex a) {
        if(a.getRealPart() == 0&&a.getImagePart() == 0) {
            System.out.println("除数不能为0");
            return new Complex();
        }
        else
            return new Complex((RealPart*a.getRealPart() - ImagePart*a.getImagePart())/(a.getRealPart()*a.getRealPart()-a.getImagePart()*a.getImagePart()),(RealPart*a.getImagePart() + ImagePart*a.getRealPart())/(a.getRealPart()*a.getRealPart()-a.getImagePart()*a.getImagePart()));
    }
}
