public class MyDoc {
    static Document D;
    static Document E;
    public static void main(String[] args) {
        D = new Document(new IntFactory());
        D.DisplayData();
        E = new Document(new LongFactory());
        E.DisplayData();
    }
}
abstract class Data {
    abstract public void DisplayValue();
}
class Integer extends Data {
    int value;
    Integer() {
        value=100;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Long extends Data {
    long value;
    Long() {
        value = 1000000000;
    }
    public void DisplayValue() {
        System.out.println(value);
    }
}
abstract class Factory {
    abstract public Data CreateDataObject();
}
class IntFactory extends Factory {
    public Data CreateDataObject(){
        return new Integer();
    }
}
class LongFactory extends Factory {
    public Data CreateDataObject(){
        return new Long();
    }
}
class Document {
    Data pd;
    Document(Factory pf){
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}