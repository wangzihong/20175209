import junit.framework.TestCase;

public class lzcComplexTest extends TestCase {
    lzccomplex a = new lzccomplex(3.0, 4.0);
    lzccomplex b = new lzccomplex(-2.0, -3.0);


    public void testEquals() {
        assertEquals(true, a.equals(a));
        assertEquals(false, b.equals(a));
    }

    public void testToString() {
        assertEquals("3.0 + 4.0i", a.toString());
        assertEquals("-2.0 - 3.0i", b.toString());
    }

    public void testComplexAdd() {
        assertEquals("1.0 + 1.0i",a.complexAdd(b).toString());
        assertEquals("-4.0 - 6.0i",b.complexAdd(b).toString());
    }

    public void testComplexSub() {
        assertEquals("5.0 + 7.0i",a.complexSub(b).toString());
        assertEquals("0",b.complexSub(b).toString());
    }

    public void testComplexMulti() {
        assertEquals("-7.0 + 24.0i",a.complexMulti(a).toString());
        assertEquals("6.0 - 17.0i",a.complexMulti(b).toString());
    }

    public void testComplexDiv() {
        assertEquals("1.0",a.complexDiv(a).toString());
    }
}