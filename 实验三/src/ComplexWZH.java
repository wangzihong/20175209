public class ComplexWZH {
    double RealPart;
    double ImagePart;
    public ComplexWZH() {}
    public ComplexWZH(double R, double I) {
        RealPart = R;
        ImagePart = I;
    }
    public double getRealPart() {
        return RealPart;
    }
    public double getImagePart() {
        return ImagePart;
    }
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        if(!(obj instanceof ComplexWZH))
            return false;
        else {
            ComplexWZH complexWZH = (ComplexWZH) obj;
            if (complexWZH.ImagePart == ((ComplexWZH) obj).ImagePart)
                return true;
            if (complexWZH.RealPart == ((ComplexWZH) obj).RealPart)
                return true;
        }
        return false;
    }
    public  String toString() {
        String str = "";
        if (RealPart == 0&&ImagePart == 0)
            str = "0.0";
        else if (RealPart == 0&&ImagePart != 0)
            str = ImagePart + "" + "i";
        else if (RealPart != 0&&ImagePart == 0)
            str = RealPart + "";
        else if (RealPart !=0&&ImagePart > 0)
            str = RealPart + "" + "+" + ImagePart + "i";
        else if (RealPart !=0&&ImagePart < 0)
            str = RealPart + "" + ImagePart + "i";
        return str;
    }
    public ComplexWZH ComplexAdd(ComplexWZH a) {
        return new ComplexWZH(RealPart + a.getRealPart(),ImagePart + a.getImagePart());
    }
    public ComplexWZH ComplexSub(ComplexWZH a) {
        return new ComplexWZH(RealPart - a.getRealPart(),ImagePart - a.getImagePart());
    }
    public ComplexWZH ComplexMulti(ComplexWZH a) {
        return new ComplexWZH(RealPart*a.getRealPart() - ImagePart*a.getImagePart(),RealPart*a.getImagePart() + ImagePart*a.getRealPart());
    }
    public ComplexWZH ComplexDiv(ComplexWZH a) {
        if(a.getRealPart() == 0&&a.getImagePart() == 0) {
            System.out.println("除数不能为0");
            return new ComplexWZH();
        }
        else
            return new ComplexWZH((RealPart*a.getRealPart() - ImagePart*a.getImagePart())/(a.getRealPart()*a.getRealPart()-a.getImagePart()*a.getImagePart()),(RealPart*a.getImagePart() + ImagePart*a.getRealPart())/(a.getRealPart()*a.getRealPart()-a.getImagePart()*a.getImagePart()));
    }
}

