/**
 * Complex
 * @author 王梓鸿
 * @date 2019/4/29
 */

public class lzccomplex {
    double realPart;
    double imagePart;

    public lzccomplex() {
    }

    public lzccomplex(double r, double i) {
        realPart = r;
        imagePart = i;
    }

    public boolean equals(lzccomplex a) {
        if  (a.realPart == this.realPart && a.imagePart == this.imagePart) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        if (this.realPart != 0 && this.imagePart > 0) {
            return this.realPart + " + " + this.imagePart + "i";
        } else if (this.realPart != 0 && this.imagePart == 0) {
            return String.valueOf(this.realPart);
        } else if (this.realPart != 0 && this.imagePart < 0) {
            return this.realPart + " - " + -this.imagePart + "i";
        } else if (this.realPart == 0 && this.imagePart != 0) {
            return this.imagePart + "i";
        } else {
            return "0";
        }
    }

    lzccomplex complexAdd(lzccomplex a) {
        return new lzccomplex(this.realPart + a.realPart, this.imagePart + a.imagePart);
    }

    lzccomplex complexSub(lzccomplex a) {
        return new lzccomplex(this.realPart - a.realPart, this.imagePart - a.imagePart);
    }

    lzccomplex complexMulti(lzccomplex a) {
        return new lzccomplex(this.realPart * a.realPart - this.imagePart * a.imagePart,
                this.imagePart * a.realPart + this.realPart * a.imagePart);
    }

    lzccomplex complexDiv(lzccomplex a) {
        return new lzccomplex((this.imagePart * a.imagePart + this.realPart * a.realPart) / (a.imagePart * a.imagePart + a.realPart * a.realPart), (this.realPart * a.imagePart - this.imagePart * a.realPart) / (a.imagePart * a.imagePart + a.realPart * a.realPart));
    }

}