import java.io.*;
import java.net.*;
import javax.crypto.*;
import java.util.Scanner;

public class Client3 {
    public static void main(String args[]) throws Exception{
        Socket mysocket;
        DataInputStream in=null;
        DataOutputStream out=null;
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.print("输入服务器的IP:");
            String IP = scanner.nextLine();
            InetAddress  address=InetAddress.getByName(IP);
            mysocket = new Socket(address, 2010);
            in = new DataInputStream(mysocket.getInputStream());
            out = new DataOutputStream(mysocket.getOutputStream());
            KeyGenerator kg = KeyGenerator.getInstance("DESede");
            kg.init(168);
            SecretKey k = kg.generateKey();
            byte []kb = k.getEncoded();
            out.writeUTF(kb.length+ "");
            System.out.println("产生的密钥为");
            for(int i=0;i<kb.length;i++) {
                System.out.print(kb[i]+ " ");
                out.writeUTF(kb[i] +"");
            }
            System.out.println("\n请输入中缀表达式：");
            while(scanner.hasNext()) {
                String question = scanner.next();
                MyBC change = new MyBC();
                change.ChangeString(question);
                String question1 = change.ChangeOrder();
                System.out.println("后缀表达式为："+question1);
                Cipher cp = Cipher.getInstance("DESede");
                cp.init(Cipher.ENCRYPT_MODE,k);
                byte ptext[] = question1.getBytes("UTF8");
                byte ctext[] = cp.doFinal(ptext);
                out.writeUTF(ctext.length + "");
                for(int i=0;i<ctext.length;i++) {
                    out.writeUTF(ctext[i] +"");
                }
                String  s=in.readUTF();   //in读取信息，堵塞状态
                System.out.println("客户收到服务器的回答:"+s);
                Thread.sleep(500);
                System.out.println("请输入中缀表达式:");
            }
        }
        catch (IOException e) {
            System.out.println("服务器已断开"+e);
        }
    }
}