import java.util.*;

public class Test {
    public static void main(String[] args) {
        String question = "";
        String question1 = "";
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入题目：");
        question = scanner.nextLine();
        MyBC change = new MyBC();
        change.ChangeString(question);
        question1 = change.ChangeOrder();
        System.out.println(question1);
        MyDC getanswer = new MyDC();
        getanswer.set(question1);
        Rational answer = getanswer.get();
        int a = answer.getNumerator();
        int b = answer.getDenominator();
        float result = (float)a/b;
        System.out.println("结果为（保留两位小数）：");
        System.out.println(String.format("%.2f",result));
    }
}
