import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Client2 {
    public static void main(String args[]) {
        Socket mysocket;
        DataInputStream in=null;
        DataOutputStream out=null;
        Scanner scanner = new Scanner(System.in);
        try{  System.out.print("输入服务器的IP:");
            String IP = scanner.nextLine();
            InetAddress  address=InetAddress.getByName(IP);
            mysocket = new Socket(address, 2010);
            in=new DataInputStream(mysocket.getInputStream());
            out=new DataOutputStream(mysocket.getOutputStream());
            System.out.println("请输入中缀表达式：");
            while (scanner.hasNext()) {
                String question = scanner.next();
                MyBC change = new MyBC();
                change.ChangeString(question);
                String question1 = change.ChangeOrder();
                System.out.println("后缀表达式为："+question1);
                out.writeUTF(question1);
                String  s=in.readUTF();   //in读取信息，堵塞状态
                System.out.println("客户收到服务器的回答:"+s);
                Thread.sleep(500);
                System.out.println("请输入中缀表达式:");
            }
        }
        catch(Exception e) {
            System.out.println("服务器已断开"+e);
        }
    }
}
