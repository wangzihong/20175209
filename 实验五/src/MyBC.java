import java.util.Stack;

public class MyBC {
    String C = new String();
    String End = "";            //存储数字的字符串

    public void ChangeString(String str) {
        C = str;
    }

    public String ChangeOrder() {
        Stack store = new Stack();     //创建一个存储字符的栈
        for (int i = 0; i < C.length(); i++) {
            char op = C.charAt(i);     //将索引值为i处的字符的值返回
            if (op >= '0' && op <= '9') {
                End = End + op;
            } else if (op == '(') {
                store.push(op);
            } else if (op == '+' || op == '-' || op == '*' || op == '÷'|| op == '/') {
                End = End + " ";
                if (store.empty()) {
                    store.push(op);
                } else if (compareValue(op) > compareValue((char) store.peek()))    //比较运算符优先级
                {
                    store.push(op);
                } else {
                    End = End + String.valueOf(store.pop());
                    i--;
                }
            } else if (op == ')') {
                while ((char) store.peek() != '(') {
                    End = End + " " + String.valueOf(store.pop());
                }
                store.pop();
            }
        }
        while (!store.empty()) {
            End = End + " " + String.valueOf(store.pop());
        }
        return End;
    }

    public int compareValue(char chi) {
        int number = 0;
        switch (chi) {
            case '(':
                number = 1;
                break;
            case '+':
            case '-':
                number = 2;
                break;
            case '*':
            case '÷':
                number = 3;
                break;
            case '/':
                number = 4;
                break;
            case ')':
                number = 5;
                break;
            default:
                number = 0;
                break;
        }
        return number;
    }
}
