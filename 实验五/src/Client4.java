import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.security.*;
import java.util.Scanner;
import  java.net.*;
public class Client4 {
    public static void main(String[] args) {
        Socket mysocket;
        DataInputStream in=null;
        DataOutputStream out=null;
        Scanner scanner = new Scanner(System.in);
        try {
            System.out.print("输入服务器的IP:");
            String IP = scanner.nextLine();
            InetAddress  address=InetAddress.getByName(IP);
            mysocket = new Socket(address, 2010);
            in = new DataInputStream(mysocket.getInputStream());
            out = new DataOutputStream(mysocket.getOutputStream());
            Key_DH.DH("Lpub.dat","Lpri.dat");
            FileInputStream my = new FileInputStream("Lpub.dat");
            ObjectInputStream mypub = new ObjectInputStream(my);
            Key kp = (Key) mypub.readObject();
            ByteArrayOutputStream DH = new ByteArrayOutputStream();
            ObjectOutputStream myDH = new ObjectOutputStream(DH);
            myDH.writeObject(kp);
            byte []pub = DH.toByteArray();
            out.writeUTF(pub.length+"");
            for(int i=0;i<pub.length;i++) {
                out.writeUTF(pub[i]+ "");
            }
            Thread.sleep(1000);
            int length = Integer.parseInt(in.readUTF());
            byte cpub[] = new byte[length];
            for(int i=0;i<length;i++) {
                String temp = in.readUTF();
                cpub[i] = Byte.parseByte(temp);
            }
            ByteArrayInputStream ckey1 = new ByteArrayInputStream(cpub);
            ObjectInputStream ckey = new ObjectInputStream(ckey1);
            Key k = (Key) ckey.readObject();
            FileOutputStream f2 = new FileOutputStream("W1pub.dat");
            ObjectOutputStream b2 = new ObjectOutputStream(f2);
            b2.writeObject(k);
            KeyAgree.Agree("W1pub.dat","Lpri.dat");
            FileInputStream f = new FileInputStream("sb.dat");
            byte[] keysb = new byte[24];
            f.read(keysb);
            System.out.println("公共密钥为：");
            for (int i = 0;i<24;i++) {
                System.out.print(keysb[i]+" ");
            }
            System.out.println("\n请输入中缀表达式：");
            while(scanner.hasNext()) {
                String question = scanner.next();
                MyBC change = new MyBC();
                change.ChangeString(question);
                String question1 = change.ChangeOrder();
                System.out.println("后缀表达式为：" + question1);
                SecretKeySpec k1 = new SecretKeySpec(keysb, "DESede");
                Cipher cp = Cipher.getInstance("DESede");
                cp.init(Cipher.ENCRYPT_MODE, k1);
                byte ptext[] = question1.getBytes("UTF8");
                byte ctext[] = cp.doFinal(ptext);
                System.out.println("加密后的后缀表达式为：");
                for (int i = 0; i < ctext.length; i++) {
                    System.out.print(ctext[i] + " ");
                }
                out.writeUTF(ctext.length + "");
                for (int i = 0; i < ctext.length; i++) {
                    out.writeUTF(ctext[i] + "");
                }
                String  s=in.readUTF();   //in读取信息，堵塞状态
                System.out.println("\n客户收到服务器的回答:"+s);
                Thread.sleep(500);
                System.out.println("请输入中缀表达式:");
            }
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }
}
