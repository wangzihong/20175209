import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.*;

public class Server4 {
    public static void main(String[] args) throws Exception {
        ServerSocket serverForClient = null;
        Socket socketOnServer = null;
        DataOutputStream out = null;
        DataInputStream in = null;
        try {
            serverForClient = new ServerSocket(2010);
        } catch (IOException e) {
            System.out.println(e);
        }
        System.out.println("等待客户呼叫");
        try {
            socketOnServer = serverForClient.accept();
            out = new DataOutputStream(socketOnServer.getOutputStream());
            in = new DataInputStream(socketOnServer.getInputStream());
            Key_DH.DH("Wpub.dat","Wpri.dat");
            int length = Integer.parseInt(in.readUTF());
            byte cpub[] = new byte[length];
            for(int i=0;i<length;i++) {
                String temp = in.readUTF();
                cpub[i] = Byte.parseByte(temp);
            }
            ByteArrayInputStream ckey1 = new ByteArrayInputStream(cpub);
            ObjectInputStream ckey = new ObjectInputStream(ckey1);
            Key k1 = (Key) ckey.readObject();
            FileOutputStream f2 = new FileOutputStream("Lpub.dat");
            ObjectOutputStream b2 = new ObjectOutputStream(f2);
            b2.writeObject(k1);
            FileInputStream my = new FileInputStream("Wpub.dat");
            ObjectInputStream mypub = new ObjectInputStream(my);
            Key kp = (Key) mypub.readObject();
            ByteArrayOutputStream DH = new ByteArrayOutputStream();
            ObjectOutputStream myDH = new ObjectOutputStream(DH);
            myDH.writeObject(kp);
            byte []pub = DH.toByteArray();
            out.writeUTF(pub.length+"");
            for(int i=0;i<pub.length;i++) {
                out.writeUTF(pub[i]+ "");
            }
            KeyAgree.Agree("Lpub.dat","Wpri.dat");
            FileInputStream f = new FileInputStream("sb.dat");
            byte[] keysb = new byte[24];
            f.read(keysb);
            System.out.println("公共密钥为：");
            for (int i = 0;i<24;i++) {
                System.out.print(keysb[i]+" ");
            }
            while(true) {
                SecretKeySpec k = new SecretKeySpec(keysb, "DESede");
                Cipher cp = Cipher.getInstance("DESede");
                cp.init(Cipher.DECRYPT_MODE, k);
                String clength = in.readUTF();
                byte ctext[] = new byte[Integer.parseInt(clength)];
                for (int i = 0; i < Integer.parseInt(clength); i++) {
                    String temp = in.readUTF();
                    ctext[i] = Byte.parseByte(temp);
                }
                byte[] ptext = cp.doFinal(ctext);
                String question = new String(ptext, "UTF8");
                System.out.print("\n解密后的后缀表达式为：" + question);
                MyDC getanswer = new MyDC();
                getanswer.set(question);
                Rational answer = getanswer.get();
                int a = answer.getNumerator();
                int b = answer.getDenominator();
                float result = (float) a / b;
                System.out.println("\n计算出的结果为"+String.format("%.2f",result));
                out.writeUTF(String.format("%.2f",result));
                Thread.sleep(500);
            }
        }
        catch(Exception e) {
            System.out.println("客户已断开" + e);
        }
    }
}